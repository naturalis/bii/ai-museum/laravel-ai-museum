#!/bin/sh

export XDEBUG_MODE=coverage
./artisan migrate
if [ -n "$DB_SEED" ]; then
    ./artisan db:seed
fi
./artisan test --coverage
