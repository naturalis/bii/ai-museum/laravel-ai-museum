<?php

use App\Http\Controllers\ContentController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\IdentificationController;
use App\Models\IdentificationModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
| */
if (env('APP_ENV') === 'local') {
    Cache::flush();
}


Auth::routes();

// Change the language
Route::post('/language', [LanguageController::class, 'setLanguage'])->name('language');

// Display the model page
Route::get('/model/{model}', [IdentificationController::class, 'show'])->name('model');

// List the classes recognized by the model
Route::get('/model/{model}/classes', [IdentificationController::class, 'listClasses'])->name('classes');

// Receive the image and return the identification
Route::post(
    '/model/{model}',
    [IdentificationController::class, 'receiveImage']
)
    ->name('upload');

// Display content page
Route::get(
    '/{locale?}/{slug?}',
    [ContentController::class, 'show']
)
    ->defaults('locale', '')
    ->defaults('slug', 'home')
    ->name('content');
