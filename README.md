Laravel AI Museum
=================

This is the laravel implementation of the AI museum collection showcase application.

To learn more about this site:

 * [museum.identify.biodiversityanalysis.nl](https://museum.identify.biodiversityanalysis.nl/)
 * [docs.aob.naturalis.io/applications/sites/museum-identification/](https://docs.aob.naturalis.io/applications/sites/museum-identification/)

## Get Started

Clone this repository.

- `mkdir -p data/database/init`
- `cp database/init/* data/database/init/`
- `sudo echo "127.0.0.1 aimuseum.dryrun.link" >> /etc/hosts`
- `cp .env.example .env`
- Set the env variables in .env

```shell
DRYRUN_ACCESS_KEY_ID=<copy paste bitwarden traefik-route53-dryrun.link access key id>
DRYRUN_SECRET_ACCESS_KEY=<copy paste bitwarden traefik-route53-dryrun.link secret access key>
```

- Install the dependencies

```shell
docker run -ti --rm --volume $PWD:/var/www registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/php-dev:latest /bin/sh -c 'composer install'
```

- Generate the app key

```shell
docker run -ti --rm --volume $PWD:/var/www registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/php-dev:latest /bin/sh -c './artisan key:generate'
```

- Create the necessary directories

```shell
mkdir -p data/storage/app/public -m 777
mkdir -p data/storage/framework/cache/data -m 777
mkdir -p data/storage/framework/sessions -m 777
mkdir -p data/storage/framework/testing -m 777
mkdir -p data/storage/framework/views -m 777
mkdir -p data/storage/logs -m 777
```

- Install npm

```shell
apt-get update && apt-get install -y npm
```

- Install the npm dependencies

```shell
npm install
```

- Run the npm build

```shell
npm run build
```

- Start the environment

```shell
docker compose up -d
```

Wait a few seconds (up to a minute) and open your browser to https://aimuseum.dryrun.link

You should now see the site's front page.

## Updating content

The content of the site resides in `database/content` in the `en` and `nl` directories.
Everything is written in markdown and is used to seed the content pages.
Whenever you want to change the content of the site you need to first change these
markdown files and to do the actual update you need to seed the database:

```
docker compose exec php-fpm /bin/sh -c './artisan db:seed ContentSeeder'
```

You can update the content of a remote server by creating a dump of the database:

```
docker compose exec database /bin/sh -c 'mysqldump --user=root --password=$MYSQL_ROOT_PASSWORD database' > database/init/init.sql
```

By stopping the remote server and delete the 'storage' directory of the database and loading
the init.sql file in the next startup you can update the content of the site.

## Linting

When developing several tools will check if your code is up to snuff.

 - [Sonar Cloud](https://sonarcloud.io/project/overview?id=naturalis_laravel-ai-museum)
 - [Parallel php lint](https://github.com/php-parallel-lint/PHP-Parallel-Lint)
 - [Code sniffer](https://github.com/squizlabs/PHP_CodeSniffer)
 - [Mess Detector](https://phpmd.org/)
 - [Larastan/PHPStan](https://github.com/larastan/larastan)

All these tools are running in the 'check' phase of the gitlab pipeline,
but these can also be run before committing and pushing code to your repository
(except for the sonar cloud linter).

```
docker run -ti --rm --volume $PWD:/var/www registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/php-dev:latest /bin/sh -c 'composer lint'
```

If you want to automatically *beautify* your code to fix potential code smells
you can also run the beautifier from the command line.

```
docker run -ti --rm --volume $PWD:/var/www registry.gitlab.com/naturalis/bii/ai-museum/docker-ai-museum/php-dev:latest /bin/sh -c 'composer format'
```

## Testing

To run the tests, you can use the following command.

```
docker compose -f docker-compose.test.yml up --abort-on-container-exit
```

This will start the database service and setup the test database
and run the tests.
The database service will be stopped after the tests are done.
These tests are exactly the same as the ones run in the gitlab pipeline.

## Bulk identification

This application also supports bulk indentification of many specimen.
To make this possible you need to have a csv file containing per line:

 - url to the downloadable image
 - image_id
 - specimen_id
 - model_name

For instance:

```csv
image_url,image_id,specimen_id,model_name
https://medialib.naturalis.nl/file/id/RMNH.INS.318369_1/format/medium,RMNH.INS.318369_1,RMNH.INS.318369,papilionoidae
https://medialib.naturalis.nl/file/id/RMNH.INS.318370_1/format/medium,RMNH.INS.318370_1,RMNH.INS.318370,papilionoidae
https://medialib.naturalis.nl/file/id/RMNH.INS.318371_1/format/medium,RMNH.INS.318371_1,RMNH.INS.318371,papilionoidae
```

Store the file in the root of this project and call the file 'specimen.csv'. And run the command:


```
docker compose exec php-fpm /bin/sh -c './artisan app:bulk-identify specimen.csv'
```

The process will download each image and send it to the model for identification,
the result of each identification is stored in another csv file.

Each of the multisource models can be used, as long as you use the exact identifier,
which can be found in the `config/service.php` file.

## Updating dependencies

To update dependencies every once in a while you need to start a composer update:

```
docker compose exec php-fpm /bin/sh -c 'composer update'
```

and npm update:

```
npm update
npm run build
```

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Pre-commit

If you want to actively work on this project, it is mandatory to install
[pre-commit](https://pre-commit.com/#install)
and [gitleaks](https://github.com/gitleaks/gitleaks) to make absolutely sure that you do not add any sensitive information
(keys, password, tokens) to this repository.

1. Install [pre-commit](https://pre-commit.com/#install)
2. Install [git-leaks](https://github.com/gitleaks/gitleaks)
3. `pre-commit install` and `pre-commit autoupdate`

The gitleaks check is also added to the gitlab pipeline.
Even if you haven't installed these tools, gitleaks will still scan your commit after a push.
Any credentials leaked this way, should be revoked, replaced and reported,
because the git history never forgets and the repository is open.
