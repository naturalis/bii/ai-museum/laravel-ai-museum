<?php

namespace App\Console\Commands;

use App\Services\MultiSourceIdentificationService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use League\CLImate\CLImate;

define("IMAGE_URL_INDEX", 0);
define("IMAGE_ID_INDEX", 1);
define("SPECIMEN_ID_INDEX", 2);
define("MODEL_NAME_INDEX", 3);
define("PREDICTION_AMOUNT", 3);
define("CSV_COLUMN_AMOUNT", 4);

class BulkIdentify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:bulk-identify {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bulk analyze images from a csv file conforming to the expected input format. ' .
        'Outputs a new CSV file.';

    /**
     * Output object for the console command.
     *
     * @var CLImate
     */
    protected $consoleOutput;

    /**
     * Execute the console command.
     */
    public function handle(CLImate $climate): int
    {
        ini_set('memory_limit', '512M');
        $this->consoleOutput = $climate;

        $filename = $this->argument("filename");
        $basename = pathinfo($filename, PATHINFO_FILENAME);

        $outputPath = $this->getOutputPath($basename, "analysed");
        $failurePath = $this->getOutputPath($basename, "failures");

        list($csvHeader, $csvData) = $this->readCsv($filename);
        if (!$this->isValidCsv($csvHeader)) {
            $this->error("Input file is not a valid csv file. Please check the format.");
            return 1;
        }

        $climate->out("Analysing $filename");
        $sortedCsvData = $this->sortCsv($csvData);
        $identifications = $this->createIdentificationsList($sortedCsvData);
        list($identifications, $failures) = $this->downloadAndRegisterImages($identifications);
        $this->handleFailures($failures, $failurePath);

        $outputHeader = [];
        $outputRows = [];

        $progress = $climate->progress()->total(count($identifications));

        $batchSize = 10;
        $batches = array_chunk($identifications, $batchSize);

        foreach ($batches as $batchIndex => $batch) {
            foreach ($batch as $index => $identification) {
                /* @var MultiSourceIdentificationService $service */
                $batchCount = count($batches);
                $progress->current(
                    $batchIndex * $batchSize + $index,
                    "Identifying specimens... Batch $batchIndex/$batchCount"
                );

                $service = $this->selectModel($identification["model_name"]);
                if (!$service) {
                    return 1;
                }
                if (!$identification["images"]) {
                    continue;
                }
                $response = $this->identifyImages($identification["images"], $service);
                $outputHeader = $this->createHeader();
                $outputRow = $this->createRow($response, $identification["specimen_id"]);

                array_push($outputRows, $outputRow);
            }
            if ($batchIndex === 0) {
                $this->writeCsvHeader($outputHeader, $outputPath);
            }
            $this->writeCsvRows($outputRows, $outputPath);
            $outputRows = [];
        }

        $this->consoleOutput->out("Done. Content written to '$outputPath'");

        return 0;
    }

    private function downloadAndRegisterImages(array $identifications): array
    {
        $progress = $this->consoleOutput->progress()->total(count($identifications));

        $failures = [];
        foreach ($identifications as &$identification) {
            $specimenId = $identification["specimen_id"];
            $images = [];
            foreach ($identification["urls"] as $index => $url) {
                $progress->current($index, "Downloading files");
                # TODO: Handle other extensions too. Don't assume .jpg.
                $imageName = $specimenId . "_" . $index . ".jpg";
                $referencePath = "images/" . $imageName;
                $storagePath = "public/" . $referencePath;
                if (!Storage::disk('local')->exists($storagePath)) {
                    try {
                        $fileContents = file_get_contents($url);
                        Storage::disk('local')->put($storagePath, $fileContents);
                    } catch (\ErrorException $exception) {
                        $this->error("'$url' was not found.");
                        array_push($failures, $url);
                        unset($identification["urls"][$index]);
                        continue;
                    }
                }
                $images[] = $referencePath;
            }
            $identification["images"] = $images;
        }

        return array($identifications, $failures);
    }

    private function selectModel(string $slug): ?object
    {
        $configPath = "services.identification.multisource.$slug";
        if (!config()->has($configPath)) {
            $this->error("Model '$slug' not found in configuration.");
            return null;
        }
        $modelConfig = config($configPath);
        $service = app("identification." . $modelConfig["service"]);

        if (!$service) {
            $this->error("Service not found for model '$slug'.");
            return null;
        }

        return $service;
    }

    private function createIdentificationsList(array $sortedCsvData): array
    {
        $identificationList = [];
        $identification = [];
        $imageUrls = [];

        foreach ($sortedCsvData as $row) {
            if (empty($identification)) {
                array_push($imageUrls, $row[IMAGE_URL_INDEX]);
                $identification["specimen_id"] = $row[SPECIMEN_ID_INDEX];
                $identification["model_name"] = $row[MODEL_NAME_INDEX];
                continue;
            }

            if ($row[SPECIMEN_ID_INDEX] === $identification["specimen_id"] &&
                $row[MODEL_NAME_INDEX] === $identification["model_name"]
            ) {
                # Specimen_id and model_name are still identical, add the image to list of urls
                array_push($imageUrls, $row[IMAGE_URL_INDEX]);
                continue;
            }

            # New specimen_id or model_name is found, store the current identification
            $identification["urls"] = $imageUrls;
            array_push($identificationList, $identification);

            # Move on the next identification
            $identification = [];
            $imageUrls = [];

            # Add the current image as a new entry
            array_push($imageUrls, $row[IMAGE_URL_INDEX]);
            $identification["specimen_id"] = $row[SPECIMEN_ID_INDEX];
            $identification["model_name"] = $row[MODEL_NAME_INDEX];
        }
        if (!empty($identification)) {
            $identification["urls"] = $imageUrls;
            array_push($identificationList, $identification);
        }

        return $identificationList;
    }


    private function identifyImages(array $images, MultiSourceIdentificationService $service): array
    {
        $response = $service->identifyMultipleImages($images);

        if (array_key_exists('error', $response)) {
            $this->info($response['error']);
            return $response;
        }

        return $response;
    }

    private function createHeader(): array
    {
        $header = [
            "Specimen",
            "Datetime",
            "Parameters",
            "Tag",
        ];
        for ($i = 0; $i < PREDICTION_AMOUNT; $i++) {
            array_push($header, "Prediction " . ($i + 1));
            array_push($header, "Probability " . ($i + 1));
        }

        return $header;
    }

    /**
     * Prepare a row for the csv file
     *
     * @param array $response The API response, sanitized and stored as array by e.g.
     *                        identifyImage or identifyMultipleImages
     * @param string $specimenId The ID associated with the identification
     *
     * @return array Array representing a csv row, in order
     */
    private function createRow(array $response, string $specimenId): array
    {
        $row = [];
        $row["specimen_id"] = $specimenId;
        $row["datetime"] = $response["generated_by"]["datetime"];
        $row["parameters"] = json_encode($response["generated_by"]["parameters"]);
        $row["tag"] = $response["generated_by"]["tag"];
        for ($i = 0; $i < PREDICTION_AMOUNT; $i++) {
            $row["prediction_" . ($i + 1)] = $response["predictions"][$i]["class"];
            $row["probability_" . ($i + 1)] = $response["predictions"][$i]["prediction"];
        }

        return $row;
    }

    /**
     * Reads the CSV file into csvHeader and csvData variables.
     *
     * @param string $filename The name of the file that will be analysed
     *
     * @return array An array with two elements: array $csvHeader and array $csvData
     */
    private function readCsv(string $filename): array
    {
        $fileHandle = fopen(storage_path($filename), 'r');
        if ($fileHandle === false) {
            $this->error("Error opening file: $filename");
            exit(1);
        }

        $csvData = [];
        $csvHeader = fgetcsv($fileHandle);
        while (($row = fgetcsv($fileHandle)) !== false) {
            $csvData[] = $row;
        }
        fclose($fileHandle);

        return [$csvHeader, $csvData];
    }

    /**
     * Rudimentary input validation. This should probably need refinement in the future.
     * e.g. check if urls are valid, valid specimen_id values, valid model_name, unique image_id, etc.
     * TODO: Improve input validation to check for more features.
     * TODO: Handle misaligned headers by reordering, and ignoring other headers.
     * TODO: Give more descriptive feedback on what exactly is wrong with the file.
     *
     * @param array $csvHeader as returned by readCsv
     *
     * @return bool returns true is the csv is suited for usage, false otherwise
     */
    private function isValidCsv(array $csvHeader): bool
    {
        if (count($csvHeader) != CSV_COLUMN_AMOUNT ||
            $csvHeader[IMAGE_URL_INDEX] != "image_url" ||
            $csvHeader[IMAGE_ID_INDEX] != "image_id" ||
            $csvHeader[SPECIMEN_ID_INDEX] != "specimen_id" ||
            $csvHeader[MODEL_NAME_INDEX] != "model_name"
        ) {
            return false;
        }
        return true;
    }

    /**
     * Sort the CSV data array based on 'model_name', followed by 'specimen_id'.
     * Sorting is needed to allow grouping of similar requests.
     *
     * @param array $csvData as returned by readCsv
     *
     * @return array sorted array
     */
    private function sortCsv(array $csvData): array
    {
        usort($csvData, function ($left, $right) {
            if ($left[MODEL_NAME_INDEX] == $right[MODEL_NAME_INDEX]) {
                return strcmp($left[SPECIMEN_ID_INDEX], $right[SPECIMEN_ID_INDEX]);
            }
            return strcmp($left[MODEL_NAME_INDEX], $right[MODEL_NAME_INDEX]);
        });

        return $csvData;
    }

    /**
     * Writes a header to a csv file
     *
     * @param array $outputHeader
     * @param string $outputPath
     *
     * @return void
     */
    private function writeCsvHeader(array $outputHeader, string $outputPath): void
    {
        $fileHandle = fopen($outputPath, 'w');
        if ($fileHandle === false) {
            $this->error("Error opening file '$outputPath'");
            exit(1);
        }
        fputcsv($fileHandle, $outputHeader);

        fclose($fileHandle);
    }

    /**
     * Writes content to a csv file
     *
     * @param array $outputRows
     * @param string $outputPath
     *
     * @return void
     */
    private function writeCsvRows(array $outputRows, string $outputPath): void
    {
        $fileHandle = fopen($outputPath, 'a');
        if ($fileHandle === false) {
            $this->error("Error opening file '$outputPath'");
            exit(1);
        }
        foreach ($outputRows as $outputRow) {
            fputcsv($fileHandle, $outputRow);
        }

        fclose($fileHandle);
    }

    /**
     * Get the full output path, given the input file.
     *
     * @param string $basename
     * @param string $suffix
     *
     * @return string
     */
    private function getOutputPath(string $basename, string $suffix): string
    {
        $outputFilename = $basename . "_" . $suffix . ".csv";
        return storage_path($outputFilename);
    }

    /**
     * Write failed URLs to a seperate output file.
     *
     * @param array $failures
     * @param string $failurePath
     *
     * @return void
     */
    private function handleFailures(array $failures, string $failurePath): void
    {
        $fileHandle = fopen($failurePath, 'w');
        if ($fileHandle === false) {
            $this->error("Error opening file '$failurePath'");
            exit(1);
        }
        fputcsv($fileHandle, ["Failed"]);
        foreach ($failures as $failure) {
            fputcsv($fileHandle, [$failure]);
        }

        fclose($fileHandle);
    }
}
