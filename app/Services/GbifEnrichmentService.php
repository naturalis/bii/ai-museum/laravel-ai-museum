<?php

namespace App\Services;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Http;

class GbifEnrichmentService extends EnrichmentService
{
    protected function getEnrichmentDataFromApi($className)
    {
        $url = $this->getEndpoint() . urlencode($className);

        try {
            $response = Http::get($url);
            $data = $response->json();
        } catch (RequestException $e) {
            // log the error
            logger()->error("Failed to retrieve data from GBIF API: " . $e->getMessage());

            return null;
        }

        return isset($data['status']) && (in_array($data['status'], ['ACCEPTED', 'HIGHERRANK']) !== false) ? [
            'url' => config('services.enrichment.gbif.species_link') . $data['usageKey'],
        ] : null;
    }
}
