<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class LinnaeusEnrichmentService extends EnrichmentService
{

    private function normalizeClassName($className)
    {
        // strip everything after and including the first '('
        $className = preg_replace('/\(.*$/', '', $className);
        // strip everything after and including the first ','
        $className = preg_replace('/\,.*$/', '', $className);

        return $className;
    }

    protected function getEnrichmentDataFromApi($className)
    {

        $url = $this->getEndpoint() . urlencode($className);

        $response = Http::get($url);

        $data = $response->json();
        if (isset($data['errors'])) {
            return null;
        }
        if (isset($data['taxon'])) {
            return [
                'url' => $data['taxon']['url'],
            ];
        }

        return null;
    }

    public function getEnrichmentData($className)
    {
        $cacheKey = $this::Class . ":" . $this->normalizeClassName($className);

        return Cache::remember($cacheKey, config('services.enrichment.cache_ttl'), function () use ($className) {
            return $this->getEnrichmentDataFromApi($className);
        });
    }
}
