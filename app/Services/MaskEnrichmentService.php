<?php

namespace App\Services;

use App\Models\MaskEnrichment;

class MaskEnrichmentService extends EnrichmentService
{
    public function __construct()
    {
        // The endpoint is not needed for the MaskEnrichmentService
    }

    private function normalizeClassName($className)
    {
        $className = preg_replace('/\s+/', '', $className);
        return strtolower($className);
    }



    public function getEnrichmentData($className)
    {
        $enrichment = MaskEnrichment::where('key', $this->normalizeClassName($className))->first();

        if ($enrichment) {
            return [
                'class' => $enrichment->class,
                'url' => $enrichment->url,
            ];
        }

        return null;
    }
}
