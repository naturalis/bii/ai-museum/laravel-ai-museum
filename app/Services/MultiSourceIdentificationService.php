<?php

namespace App\Services;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class MultiSourceIdentificationService extends IdentificationService
{
    protected $baseUrl;
    protected $timeout;
    protected $enriched;
    protected mixed $username;
    protected mixed $password;
    protected mixed $token;

    public function __construct($config)
    {
        $this->baseUrl = $config['api'];
        $this->username = array_key_exists('username', $config) ? $config['username'] : '';
        $this->password = array_key_exists('password', $config) ? $config['password'] : '';
        $this->token = array_key_exists('token', $config) ? $config['token'] : '';
        $this->timeout = array_key_exists('timeout', $config) ? $config['timeout'] : 30;
        $this->enriched = false;
    }

    public function isApiReachable(): bool
    {
        $pingUrl = $this->baseUrl . "/ping";
        try {
            $response = Http::timeout($this->timeout)->get($pingUrl);

            return $response->successful() && ($response->json('error') === null);
        } catch (RequestException $e) {
            return false;
        }
    }

    public function identifyImage(string $filepath): array
    {
        $identifyUrl = $this->baseUrl . "/identify";
        if ($this->token) {
            $identifyUrl = $this->baseUrl . "/identify/token/" . $this->token;
        }

        try {
            /* @var \Illuminate\Http\Client\Response $response */
            $response = Http::timeout($this->timeout);
            if ($this->username !== '' && $this->password !== '') {
                $identifyUrl = $this->baseUrl . "/identify/auth";
                $response = $response->withBasicAuth($this->username, $this->password);
            }
            $response = $response->attach(
                'image',
                file_get_contents(storage_path('app/public/' . $filepath)),
                basename($filepath)
            )->post($identifyUrl);

            if ($response->failed()) {
                return ['error' => __('Failed to identify image.')];
            }

            return $this->parsedResponse($response);
        } catch (RequestException $e) {
            return ['error' => __('Failed to communicate with the API.')];
        }
    }

    public function identifyMultipleImages(array $filepaths): array
    {
        $identifyUrl = $this->baseUrl . "/identify";
        if ($this->token) {
            $identifyUrl = $this->baseUrl . "/identify/token/" . $this->token;
        }

        try {
            /* @var \Illuminate\Http\Client\Response $response */
            $response = Http::timeout($this->timeout);
            if ($this->username !== '' && $this->password !== '') {
                $identifyUrl = $this->baseUrl . "/identify/auth";
                $response = $response->withBasicAuth($this->username, $this->password);
            }
            foreach ($filepaths as $filepath) {
                $response = $response->attach(
                    'image',
                    file_get_contents(storage_path('app/public/' . $filepath)),
                    basename($filepath)
                );
            }

            $response = $response->post($identifyUrl);

            if ($response->failed()) {
                return ['error' => __('Failed to identify image.')];
            }

            return $this->parsedResponse($response);
        } catch (RequestException $e) {
            return ['error' => __('Failed to communicate with the API.')];
        }
    }

    private function getGbifId($scientificId): string
    {
        $gbifId = '';
        if (isset($scientificId)) {
            $gbifId = explode(':', $scientificId)[1];
        }
        return $gbifId;
    }

    private function normalizePredictions(array $predictions): array
    {
        $normalized = [];
        if (isset($predictions[0])) {
            $predictions = $predictions[0];
        }
        $taxa = $predictions['taxa']['items'] ?? [];
        foreach ($taxa as $taxon) {
            $prediction = [
                'class' => $taxon['scientific_name'],
                'prediction' => ($taxon['probability']),
            ];
            $gbifId = '';
            if (isset($taxon['scientific_name_id_shared'])) {
                $gbifId = $this->getGbifId($taxon['scientific_name_id_shared']);
            }
            if ($gbifId) {
                $this->enriched = true;
                $prediction['enrichments'] = [
                    'gbif' => [
                        'url' => 'https://www.gbif.org/species/' . $gbifId,
                    ],
                ];
            }

            $normalized[] = $prediction;
        }
        return $normalized;
    }

    private function parsedResponse(Response $response): array
    {
        $json = $response->json();
        if (isset($json['predictions'])) {
            $json['predictions'] = $this->normalizePredictions($json['predictions']);
        }
        $json['enriched'] = $this->enriched;
        return $json;
    }

    private function normalizeClasses(Response $response): array
    {
        $json = $response->json();
        $taxa = $json['taxa'] ?? [];

        $classes = [];
        foreach ($taxa as $taxon) {
            $classes[] = $taxon['name'];
        }
        sort($classes);

        return ['classes' => $classes];
    }

    public function retrieveClasses(): array
    {
        $classesUrl = $this->baseUrl . "/taxa";

        try {
            $response = Http::timeout($this->timeout)->get($classesUrl);
            if ($response->failed()) {
                return ['error' => __('Failed to retrieve classes.')];
            }
            return $this->normalizeClasses($response);
        } catch (RequestException $e) {
            return ['error' => __('Failed to communicate with the API.')];
        }
    }
}
