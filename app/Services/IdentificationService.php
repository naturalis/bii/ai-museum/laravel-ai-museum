<?php

namespace App\Services;

use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Http;

class IdentificationService
{

    protected $baseUrl;
    protected $timeout;

    private const DEFAULT_TIMEOUT = 30;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
        $this->timeout = config('services.identification_timeout', self::DEFAULT_TIMEOUT);
    }

    public function setUrl(string $url): void
    {
        $this->baseUrl = $url;
    }

    public function isApiReachable(): bool
    {
        $classesUrl = $this->baseUrl . "/classes";
        try {
            $response = Http::timeout($this->timeout)->get($classesUrl);

            return $response->successful() && ($response->json('error') === null);
        } catch (RequestException $e) {
            return false;
        }
    }

    public function identifyImage(string $filepath): array
    {
        $identifyUrl = $this->baseUrl . "/identify";

        try {
            /* @var \Illuminate\Http\Client\Response $response */
            $response = Http::timeout($this->timeout)->attach(
                'image',
                file_get_contents(storage_path('app/public/' . $filepath)),
                basename($filepath)
            )->post($identifyUrl);

            if ($response->failed()) {
                return ['error' => __('Failed to identify image.')];
            }
            return $response->json();
        } catch (RequestException $e) {
            return ['error' => __('Failed to communicate with the API.')];
        }
    }

    public function retrieveClasses(): array
    {
        $classesUrl = $this->baseUrl . "/classes";

        try {
            $response = Http::timeout($this->timeout)->get($classesUrl);
            if ($response->failed()) {
                return ['error' => __('Failed to retrieve classes.')];
            }
            return $response->json();
        } catch (RequestException $e) {
            return ['error' => __('Failed to communicate with the API.')];
        }
    }
}
