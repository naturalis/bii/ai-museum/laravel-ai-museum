<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class EnrichmentService
{
    protected string $endpoint = '';

    public function __construct(string $endpoint)
    {
        $this->endpoint = $endpoint;
    }

    protected function getEndpoint(): string
    {
        return $this->endpoint;
    }

    protected function getEnrichmentDataFromApi($className)
    {
        $url = $this->getEndpoint() . urlencode($className);

        $response = Http::get($url);

        return $response->json();
    }

    public function getEnrichmentData($className)
    {
        $cacheKey = $this::Class . ":" . $className;

        return Cache::remember($cacheKey, config('services.enrichment.cache_ttl'), function () use ($className) {
            return $this->getEnrichmentDataFromApi($className);
        });
    }
}
