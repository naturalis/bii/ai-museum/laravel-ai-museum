<?php

namespace App\Http\Controllers;

use App\Models\Content;
use App\Models\IdentificationModel;
use App\Services\EnrichmentService;
use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class IdentificationController extends Controller
{
    /**
     * The model used for handling identification.
     *
     * @var IdentificationModel
     */
    protected $model;

    /**
     * Enriches the prediction with data from the enrichment services.
     *
     * @param mixed $prediction
     * @return mixed
     */
    private function enrichPrediction(IdentificationModel $model, mixed $prediction): mixed
    {

        /**
         * Traverse through the enrichment services and enrich the prediction with the data.
         *
         * @var EnrichmentService $enrichmentService
         */
        foreach ($model->enrichmentApis() as $enrichmentName => $enrichmentService) {
            $enrichmentData = $enrichmentService->getEnrichmentData($prediction['class']);
            if (!empty($enrichmentData)) {
                if (!isset($prediction['enrichments'])) {
                    $prediction['enrichments'] = [];
                }
                $prediction['enrichments'][$enrichmentName] = $enrichmentData;
            }
        }
        return $prediction;
    }

    private function enrichPredictions(IdentificationModel $model, array $predictions): array
    {
        $enrichedPredictions = [];

        foreach ($predictions as $prediction) {
            $enrichedPredictions[] = $this->enrichPrediction($model, $prediction);
            if (count($enrichedPredictions) >= config('app.predictions.limit')) {
                break;
            }
        }

        return $enrichedPredictions;
    }

    public function show(IdentificationModel $model): View
    {
        $locale = session()->get('locale') ?? app()->getLocale();
        $slug = "/model/{$model->slug}";

        $content = Content::where('slug', $slug)->first();

        $title = $content->getTranslation('title', $locale) ?? $model->name;
        $body = $content->getTranslation('body', $locale) ?? '';

        $apiReachable = $model->identificationApi()->isApiReachable();
        $restricted = $model->isRestricted();

        return view('model', compact('model', 'locale', 'title', 'body', 'apiReachable', 'restricted'));
    }

    public function listClasses(IdentificationModel $model): View
    {
        $locale = session()->get('locale') ?? app()->getLocale();

        $title = $model->getTitle($locale);
        $classes = $model->identificationApi()->retrieveClasses();

        if (isset($classes['error']) || empty($classes)) {
            $error = $classes['error'] ?? __('Failed to retrieve classes.');

            return view('classes', compact('model', 'title', 'error'));
        }

        return view('classes', compact('model', 'title', 'classes'));
    }

    public function receiveImage(IdentificationModel $model, Request $request): JsonResponse
    {
        if ($model->isRestricted()) {
            return response()->json(['error' => __('This model is restricted')]);
        }

        $request->validate(['image' => 'required|image']);

        $storePath = $request->file('image')->store('images', 'public');

        $result = $model->identificationApi()->identifyImage($storePath);

        if (isset($result['error'])) {
            return response()->json($result);
        }
        if (empty($result) || !isset($result['predictions'])) {
            return response()->json(['error' => __('Failed to identify the image')]);
        }

        $result['predictions'] = $this->enrichPredictions($model, $result['predictions']);
        if (empty($result['enriched'])) {
            $result['enriched'] = $model->hasEnrichmentServices();
        }

        return response()->json($result);
    }
}
