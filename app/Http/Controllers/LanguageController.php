<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class LanguageController extends Controller
{
    public function setLanguage(Request $request): JsonResponse
    {
        try {
            $request->validate([
                'locale' => 'required|string|size:2',
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'errors' => $e->errors(),
            ], 400);
        }

        $locale = $request->input('locale');
        if (in_array($locale, config('app.locales')) === false) {
            return response()->json([
                'errors' => [
                    'locale' => 'Invalid locale',
                ],
            ], 400);
        }

        session(['locale' => $locale]);
        app()->setLocale($locale);

        return response()->json([
            'success' => true,
            'language' => $locale,
        ]);
    }
}
