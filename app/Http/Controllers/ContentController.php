<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use App\Models\Content;

class ContentController extends Controller
{
    public function show(String $locale, String $slug): View
    {
        if ($locale == '') {
            $locale = session()->has('locale') ? session()->get('locale') : app()->getLocale();
        }

        $content = Content::where('slug', $slug)->firstOrFail();
        session()->put('locale', $locale);

        $title = $content->getTranslation('title', $locale);
        $body = $content->getTranslation('body', $locale);

        return view('page', compact('title', 'body', 'locale'));
    }
}
