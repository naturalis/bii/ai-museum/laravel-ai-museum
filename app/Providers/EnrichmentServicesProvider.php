<?php

namespace App\Providers;

use App\Services\GbifEnrichmentService;
use App\Services\LinnaeusEnrichmentService;
use App\Services\MaskEnrichmentService;
use Illuminate\Support\ServiceProvider;

class EnrichmentServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind('enrichment.gbif', function () {
            return new GbifEnrichmentService(config('services.enrichment.gbif.search'));
        });
        $this->app->bind('enrichment.nederlandsesoorten', function () {
            return new LinnaeusEnrichmentService(config('services.enrichment.nederlandsesoorten.search'));
        });
        $this->app->bind('enrichment.conidae', function () {
            return new LinnaeusEnrichmentService(config('services.enrichment.conidae.search'));
        });
        $this->app->bind('enrichment.malesianbutterflies', function () {
            return new LinnaeusEnrichmentService(config('services.enrichment.malesianbutterflies.search'));
        });
        $this->app->bind('enrichment.masks', function () {
            return new MaskEnrichmentService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
