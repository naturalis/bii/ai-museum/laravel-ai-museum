<?php

namespace App\Providers;

use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\ServiceProvider;

class BroadcastServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function boot(): void
    {
        Broadcast::routes();

        require_once base_path('routes/channels.php');
    }
}
