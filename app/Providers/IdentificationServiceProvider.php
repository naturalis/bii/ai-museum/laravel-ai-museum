<?php

namespace App\Providers;

use App\Services\IdentificationService;
use App\Services\MultiSourceIdentificationService;
use Illuminate\Support\ServiceProvider;

class IdentificationServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind('identification.museum_v1', function () {
            return new IdentificationService(config('services.identification.museum_v1.api'));
        });

        $services = config('services.identification.multisource');
        foreach ($services as $serviceConfig) {
           if (is_array($serviceConfig) && array_key_exists('service', $serviceConfig)) {
               $this->app->bind('identification.' . $serviceConfig['service'], function () use ($serviceConfig) {
                   return new MultiSourceIdentificationService($serviceConfig);
               });
           }
        }
    }

}
