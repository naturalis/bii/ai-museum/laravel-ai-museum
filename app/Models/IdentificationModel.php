<?php

namespace App\Models;

use App\Services\IdentificationService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IdentificationModel extends Model
{
    use HasFactory;

    /**
     * Returns the configured identification API service.
     *
     * @return IdentificationService
     */
    public function identificationApi()
    {
        /**
         * Create a new instance of the identification service.
         *
         * @var IdentificationService $api
         */
        $api = app("identification." . $this->identification_service);
        $api->setUrl($this->api_url);

        return $api;
    }

    public function enrichmentApis(): array
    {
        $enrichmentServices = json_decode($this->enrichments, true);

        $enrichmentApis = [];
        foreach ($enrichmentServices as $service => $enabled) {
            if ($enabled) {
                try {
                    $enrichmentApis[$service] = app("enrichment.$service");
                } catch (BindingResolutionException $e) {
                    logger()->error("Enrichment service $service does not exist");
                }
            }
        }

        return $enrichmentApis;
    }

    public function hasEnrichmentServices(): bool
    {
        $enrichmentServices = json_decode($this->enrichments, true);

        foreach ($enrichmentServices as $enabled) {
            if ($enabled) {
                return true;
            }
        }

        return false;
    }

    public function getTitle($locale): string
    {
        $titles = json_decode($this->title, true);

        return $titles[$locale];
    }

    public function isRestricted(): bool
    {
        $settings = json_decode($this->settings, true);

        return isset($settings['restricted']) && $settings['restricted'];
    }
}
