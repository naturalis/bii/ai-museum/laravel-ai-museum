<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Content extends Model
{
    use HasFactory;
    use HasTranslations;

    public $translatable = ['title', 'body'];

    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    protected $fillable = ['title', 'slug', 'body'];
}
