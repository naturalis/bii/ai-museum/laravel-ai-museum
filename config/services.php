<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'https',
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'enrichment' => [
        'gbif' => [
            'search' => 'https://api.gbif.org/v1/species/match?name=',
            'species_link' => 'https://www.gbif.org/species/'
        ],
        'nederlandsesoorten' => [
            'search' => 'https://www.nederlandsesoorten.nl/linnaeus_ng/app/views/webservices/taxon.php?pid=1&taxon='
        ],
        'conidae' => [
            'search' => 'https://conidae.linnaeus.naturalis.nl/linnaeus_ng/app/views/webservices/taxon.php?pid=1&taxon='
        ],
        'malesianbutterflies' => [
            'search' => 'https://malesianbutterflies.linnaeus.naturalis.nl/linnaeus_ng/app/views/webservices/taxon.php?pid=1&taxon='
        ],
        'masks' => [
            'search' => 1
        ],
        'cache_ttl' => (60 * 60 * 24)
    ],
    'identification' => [
        'museum_v1' => [
            'api' => 'https://museum.api.biodiversityanalysis.nl/api/',
        ],
        'multisource' => [
            'papilionoidae' => [
                'selector' => 'papilionoidae',
                'model' => 'legacy-vlinder-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-vlinders-v2',
                'title' => [
                    'en' => 'Southeast Asian butterflies (Papilionoidea species level)',
                    'nl' => 'Zuidoost-Aziatische dagvlinders (Papilionoidea soort niveau)'
                ],
                'enrichments' => ['malesianbutterflies' => 1, 'gbif' => 1],
                'service' => 'legacy-vlinder-v2'
            ],
            'papilionoidae_genera' => [
                'selector' => 'papilionoidae_genera',
                'model' => 'legacy-vlinder-genera-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-vlinder-genera-v2',
                'title' => [
                    'en' => 'Southeast Asian butterflies (Papilionoidea genera level)',
                    'nl' => 'Zuidoost-Aziatische dagvlinders (Papilionoidea genera niveau)'
                ],
                'enrichments' => ['malesianbutterflies' => 1, 'gbif' => 1],
                'service' => 'legacy-vlinder-genera-v2'
            ],
            'papilionoidae_subfamilies' => [
                'selector' => 'papilionoidae_subfamilies',
                'model' => 'legacy-vlinder-subfamilie-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-vlinder-subfamilie-v2',
                'title' => [
                    'en' => 'Southeast Asian butterflies (Papilionoidea subfamily level)',
                    'nl' => 'Zuidoost-Aziatische dagvlinders (Papilionoidea subfamilie niveau)'
                ],
                'enrichments' => ['malesianbutterflies' => 1, 'gbif' => 1],
                'service' => 'legacy-vlinder-subfamilie-v2'
            ],
            'conidae' => [
                'selector' => 'conidae',
                'model' => 'legacy-conidae-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-conidae-v2',
                'title' => [
                    'en' => 'Cone shells (Conidae)',
                    'nl' => 'Huisjes van kegelslakken (Conidae)'
                ],
                'enrichments' => ['conidae' => 1, 'gbif' => 1],
                'service' => 'legacy-conidae-v2'
            ],
            'maskers' => [
                'selector' => 'maskers',
                'model' => 'legacy-maskers-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-maskers-v2',
                'title' => [
                    'en' => 'Ethnographic masks',
                    'nl' => 'Maskers uit verschillende culturen'
                ],
                'enrichments' => ['masks' => 1],
                'service' => 'legacy-maskers-v2'
            ],
            'maskers_continent' => [
                'selector' => 'maskers_continent',
                'model' => 'legacy-maskers-continent-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-maskers-continent-v2',
                'title' => [
                    'en' => 'Ethnographic masks (continent level)',
                    'nl' => 'Maskers uit verschillende culturen (continent niveau)'
                ],
                'enrichments' => ['masks' => 1],
                'service' => 'legacy-maskers-continent-v2'
            ],
            'haaien_roggen_eieren' => [
                'selector' => 'haaien_roggen_eieren',
                'model' => 'legacy-shark-eggs-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-shark-eggs-v2',
                'title' => [
                    'en' => 'Ray and shark eggs',
                    'nl' => 'Roggen- en haaieneieren'
                ],
                'enrichments' => ['gbif' => 1],
                'service' => 'legacy-shark-eggs-v2'
            ],
            'vogeleieren' => [
                'selector' => 'vogeleieren',
                'restricted' => true,
                'model' => 'legacy-bird-eggs-v2',
                'api' => env('MULTISOURCE_BASE_URL', 'https://dev-api-multisource-beeldherkenning.hosts.naturalis.io') . '/legacy-bird-eggs-v2',
                'title' => [
                    'en' => 'Bird Eggs (restricted)',
                    'nl' => 'Vogeleieren (beperkt)'
                ],
                'service' => 'legacy-bird-eggs-v2'
            ]
        ]
    ]
];
