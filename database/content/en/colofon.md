This web application was developed as part of the project *Automatic image recognition as a tool for museum collections: innovation project in Dutch natural history collections* (2019-2022).

In this project, Naturalis Biodiversity Center researched with partners:

* [Museon-Omniversum](https://oneplanet.nl/)
* [Natuurhistorisch Museum Rotterdam](https://www.hetnatuurhistorisch.nl/)
* [Natuurmuseum Brabant](https://www.natuurmuseumbrabant.nl/)
* [Rijksmuseum](https://www.rijksmuseum.nl/nl)

The applicability of automatic image recognition based on *machine learning* for museum collections. This included a specific focus on natural history collections.

This project was made possible in part by:

* [Mondriaan Fonds](https://www.mondriaanfonds.nl/)
* [Prins Bernhard Cultuurfonds](https://www.cultuurfonds.nl/)
* [Netherlands Biodiversity Information Facility (NLBIF)](https://www.nlbif.nl/)

## Project coordination

Naturalis Biodiversity Center

## Application development

Naturalis Biodiversity Center

## Recognition models - machine learning

Naturalis Biodiversity Center

## Recognition models - photos for training

* Naturalis Biodiversity Center
* Natuurhistorisch Museum Rotterdam
* Museon-Omniversum
* Collectie Fabrice Prugnaud (French collection)
* National History Museum London (collection GBIF)
* Muséum national d'Histoire naturelle, Paris (collection GBIF)

## Texts / additional information

* Naturalis Biodiversity Center (natuurhistorische collections)
* Museon-Omniversum (maskers)
