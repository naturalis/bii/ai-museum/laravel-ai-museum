What species of butterfly did I digitize?

What ray egg case did I find on the beach?

What culture does this mask in my collection belong to?

This web application, based on artificial intelligence (specifically **machine learning**),
can help you answer such identification questions automatically.

On input, a model will *always* give three results,
the three most similar classes that the model contains.
Thus, the model cannot indicate that it does not know something.

To verify the identifications provided by the models,
a link to additional information has been added.
 

Image recognition models are currently available for the following sub-collections:

* [Southeast Asian butterflies (Papilionoidea species level)](/model/papilionoidae)
* [Southeast Asian butterflies (Papilionoidea genera level)](/model/papilionoidae_genera)
* [Southeast Asian butterflies (Papilionoidea subfamily level)](/model/papilionoidae_subfamilies)
* [Cone shells (Conidae)](/model/conidae)
* [Ethnographic masks](/model/maskers)
* [Ethnographic masks (continent level)](/model/maskers_continent)
* [Ray and shark egg cases](/model/haaien_roggen_eieren)
* [Bird eggs](/model/vogeleieren) (currently restricted for public use)

This experimental web application is a result of the project Automatic image recognition
as a tool for museum collections:
innovation project in the Dutch natural history collections (2019-2022).
For more information about this project,
see the colophon and the project page on the [Naturalis website](https://www.naturalis.nl/wetenschap/beeldherkenning-als-instrument-voor-museumcollecties).
