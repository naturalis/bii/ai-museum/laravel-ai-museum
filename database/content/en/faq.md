**What is the maximum file size for the images?**

The maximum file size for photos to be identified via the drag and drop is 10 MB.

**What do the results mean?**

The model always gives three identifiers that most closely resemble classes known to the model. The height of the prediction is displayed in a number from 0 to 1, with a color code. (green is high prediction, orange moderate, red low).

**Which classes can be recognized?**

For each model it is indicated with which classes it has been trained (depending on the model the species, genera or subfamilies or types of objects). You can request this by clicking 'all (...) in this model' at the top of the page of the relevant model.

**I'm sure the determination is wrong, why do three results appear?**

The model will always generate three results that most closely resemble the entered image, even if it is a completely different type of object.

**How can I verify the results?**

A link to additional information about the type or type of object appears below the results.

**Where can I find additional information about the kind or type of object the model suggests?**

Below the results are links to additional information - validated by Naturalis - about the type or type of object. A pop-up with information appears after clicking the i icon. Depending on the model, there are also links to additional information on, for example, GBIF and the Dutch Register of Species. 

**Why am I getting an error message or no results?**

The uploaded file may be corrupt, too large, or too much time has passed between file upload and server response. 
Check if your file is a valid image (eg a jpeg), no larger than 10mb and then try again. 
Sometimes it also helps to clear the cache of your web browser.
