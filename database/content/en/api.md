All models are also available through a publicly available API which can be publicly reached via:

`https://museum.api.biodiversityanalysis.nl/`

List of the currently available models:

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th class="col-5">Model</th>
            <th class="col-7">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>legacy-vlinders-v2</code></td>
            <td>Butterflies from the papillot collection with south-east asian butterflies specimens.</td>
        </tr>
        <tr>
            <td><code>legacy-vlinder-genera-v2</code></td>
            <td>The butterfly model recognizing images on genera level.</td>
        </tr>
        <tr>
            <td><code>legacy-vlinder-subfamilie-v2</code></td>
            <td>The butterfly model recognizing images on subfamily level.</td>
        </tr>
        <tr>
            <td><code>legacy-conidae-v2</code></td>
            <td>Cone shells.</td>
        </tr>
        <tr>
            <td><code>legacy-maskers-v2</code></td>
            <td>Masks from the museon collection.</td>
        </tr>
        <tr>
            <td><code>legacy-maskers-continent-v2</code></td>
            <td>Masks from the museum collection grouped by continent.</td>
        </tr>
        <tr>
            <td><code>legacy-shark-eggs-v2</code></td>
            <td>Shark and ray eggs.</td>
        </tr>
        <tr>
            <td><code>legacy-bird-eggs-v2</code></td>
            <td>Bird eggs and egg shells (restricted).</td>
        </tr>
    </tbody>
</table>


## Endpoints

And this API has for separate model the following endpoints.
In this example the `legacy-vlinders-v2` model is used,
but you can pick any model from the table above.

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th class="col-5">Description</th>
            <th class="col-1">Method</th>
            <th class="col-6">Endpoint</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Base</td>
            <td>GET </td>
            <td><code>/</code></td>
        </tr>
        <tr>
            <td>Ping to check API</td>
            <td>GET </td>
            <td><code>/legacy-vlinders-v2/ping</code></td>
        </tr>
        <tr>
            <td>Documentation of model api</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/documentation</code></td>
        </tr>
        <tr>
            <td>Listed endpoints</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/endpoints</code></td>
        </tr>
        <tr>
            <td>Identify image</td>
            <td>POST</td>
            <td><code>/legacy-vlinders-v2/identify</code></td>
        </tr>
        <tr>
            <td>Authenticate</td>
            <td>POST</td>
            <td><code>/legacy-vlinders-v2/identify/auth</code></td>
        </tr>
        <tr>
            <td>Identify image using token</td>
            <td>POST</td>
            <td><code>/legacy-vlinders-v2/identify/token/{token}</code></td>
        </tr>
        <tr>
            <td>Classess recognized</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/taxa</code></td>
        </tr>
        <tr>
            <td>Classess recognized (filter)</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/taxa/all?id={taxon_id}</code></td>
        </tr>
    </tbody>
</table>

### Root endpoint

The front page of the API, containing some basic information.

URL: `https://museum.api.biodiversityanalysis.nl/`

Returns: Html page about the API.

Note that the contents of this content is currently not up to date.
This is one of the issues that will be resolved in the near future.

### /ping

To test if a specific model loaded and ready for identification, you can *ping* the model.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/ping`

Parameters: none

Methods: GET

Returns: *pong*

### /documentation

To get a json representation of documentation and configuration of a certain model.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/documentation`

Methods: GET

Returns: json structure

### /taxa

Returns an alphabetical list of all classes that a model contains ('recognized').

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/taxa`

Parameters: none

Methods: GET

### /identify

Endpoint to identify an image with.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify`

Parameters: `image=` (multipart/form-data); Methods: POST

Example using cURL:

```
curl -XPOST -F "image=@butterfly.jpg" \
  https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify
```

The response contains a 'predictions' field, which in turn contains
a JSON-encoded list of taxa recognized in the uploaded image.
Each prediction consists of a class and the probability that it involves that class (as a fraction of 1).
The API always returns the predictions for all classes in the model, ordered descending by probability.

## /identify/auth

A number of models are not publicly accessible or restricted.
These are identified by the tag 'restricted' in the output of the models endpoint.
To use these models, please contact Naturalis.
If you have permission to use the corresponding endpoints use,
then you should have been given a username and password provided for access.
The authorization has been implemented as Basic Authorization.
Example implementation using cURL,
if you have the username 'my_name' and password 'secret' are given:

```
curl -u "my_name:secret" \
  https://museum.api.biodiversityanalysis.nl/legacy-bird-eggs-v2/identify/auth
```

### /identify/{token}

Endpoint to identify an image with, using a token supplied by Naturalis.
This can also be used for models that have restricted access or limited requests per period.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify/token/{token}`

Parameters: `image=` (multipart/form-data)

Methods: POST

Example using cURL:

```
curl -XPOST -F "image=@butterfly.jpg" \
  https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify/token/{token}
```

