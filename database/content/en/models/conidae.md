The cone shell recognition model was trained with photographs of 797 species of cone shells (Conidae). The images of the determined species with which the model was trained came from four museum collections and one private collection.

**Images training model**

The model was trained with photographs of cone shells against both dark and light backgrounds. The number of specimens in the photograph varies; many photographs contain one or two specimens, but the training set includes photographs with multiple specimens.

Depending on the collection, the photos contain one or more labels in addition to the specimen(s).

**Training Data**

Images and identifications are from:

* Naturalis Biodiversity Center
* Natural History Museum Rotterdam
* National History Museum London (GBIF collection)
* Muséum national d'Histoire naturelle, Paris (GBIF collection)
* Private collection of Fabrice Prugnaud (France)

A minimum of two images per species (class) was used for this model.

To unify the scientific naming of species from the different collections, GBIF and WoRMS were used.

Examples of training data:

<p>
  <img src="/images/conid2.jpg" alt="Conid" style="display:inline-block; margin-right:10px;">
  <img src="/images/conus.jpg" alt="Conidae" style="display:inline-block;">
</p>
