The recognition model for the egg capsules of rays and sharks was trained using 234 photographs of 11 species of rays and 2 species of sharks found along the Dutch coast.
The egg capsules, which can be found on Dutch beaches, are from the Naturalis collection and were photographed in a dried state.

**Training dates**: The model was trained with 234 images of a total of 13 classes (caps of 11 species of rays and 2 species of sharks).
The minimum number of images per class is 2. In addition to the specimen(s), the photographs include one one or more tags, a ruler and a color chart.

**Additional information** appears with the determinations as a link to the species on the Dutch species register. This information on the rays and sharks also contains information on the egg capsules. 

This information can also be found through the **Species Finder Egg Capsules Rays and Sharks**. To determine fresh egg capsules, you can also use [ObsIdentify](https://waarneming.nl/apps/obsidentify/) and the Dutch Shark Society's [Determination Guide for Egg Capsules of Sharks, Fleets and Rays](https://www.dutchsharksociety.org/wp-content/uploads/2022/01/DeGroteEikapselJacht.pdf).

Example of photo in training data:

![Eikapsels](/images/rog-ei.jpg)
