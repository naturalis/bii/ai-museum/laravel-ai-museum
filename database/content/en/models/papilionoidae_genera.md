This model with which Papilionoidea are recognized at the genera level is not trained on identified species, but on the label texts of unidentified, digitized specimens from the Papillotten collection. The data with which this model was trained is therefore more generic than the model Southeast Asian butterflies (Papilionoidea), on the other hand, this model is trained with a larger data set concerning genera than the model at species level.

This model can be used to sort butterflies by (possible) genera.
