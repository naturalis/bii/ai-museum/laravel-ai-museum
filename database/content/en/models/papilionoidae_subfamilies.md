This model, which recognizes Papilionoidea at the level of subfamilies, is not trained with identified species, but on the label texts of unidentified, digitized specimens from Naturalis Papillotten collection. The data with which this model was trained is therefore more generic, on the other hand this model is trained with a larger data set (support) concerning subfamilies than the model at species level.

This model can be used to sort the butterflies on (possible) subfamilies.
