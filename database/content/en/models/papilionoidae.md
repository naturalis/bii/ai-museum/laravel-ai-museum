**Species model Papilionoidea**

Several models have been trained for the folded butterflies in the Naturalis collection, the so-called 'papillot collection'. 
The models were trained with photos of folded specimens of mainly Southeast Asian butterflies from the Naturalis collection. 

This species-level model has been trained using specialist-identified specimens, and higher taxonomic-level models have been trained using label texts to sort collections by subfamily or genus level. (below)

**Images training model**

![Papillot](/images/papillot.jpg)

Only the underside of one of the wings of these specimens has been photographed, and this model will therefore not be able to recognize butterflies from images of the upper side of the wing. For certain species, additional information will therefore have to be consulted in order to be able to identify the species. In addition to the folded specimens, the photos on which the model was trained also contain labels, the original packaging (papillot) and a QR code on a Naturalis label with the collection number. 

**Training data**

A name resolver using GBIF was used for uniform naming. 

**Species information for the identifications**

Additional, English species information appears with the identifications as a link, which refers to the project [Malesian Butterflies](https://malesianbutterflies.linnaeus.naturalis.nl/linnaeus_ng/app/views/introduction/topic.php?id=19&epi=1).
