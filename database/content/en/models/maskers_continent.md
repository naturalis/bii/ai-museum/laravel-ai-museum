The model to recognize masks was trained using 75 types of masks from different cultures from West and Central Africa, North and Central America, Asia and Oceania, taken from Museon-Omniversum's collection. Museon-Omniversum's collection here served as a starting point for the amount of different types of masks. The reference images come from the collection of the National Museum of World Cultures, but also from various online sources, such as ethnographic publications and auction sites, which were collected and reviewed by Museon curators.

**Images training model**

Photographs of the masks are usually frontal, "en face" but sometimes oblique, lying down.
The background color varies from white to very dark, and from pale green to orange-red.

![Masker](/images/kavat-mask.jpg)

**Training Data**

The dataset consists of masks with a multitude of features, mostly ritual, but certain dance and theater masks are also part of the set.

Disclaimer: Image recognition is based on visual characteristics of the masks. Sometimes the same features occur in different cultures, then the result will be less precise or even incorrect. Based on image recognition, no statements can be made about the authenticity and value of a mask.
