Het model om maskers te herkennen is getraind aan de hand van 75 typen maskers afkomstig uit verschillende culturen uit West- en Centraal Afrika, Noord- en Midden-Amerika, Azië en Oceanië, afkomstig uit de collectie van Museon-Omniversum. De collectie van het Museon-Omniversum diende hierbij als uitgangspunt voor de hoeveelheid verschillende typen maskers. De referentiefoto’s komen o.a. uit de collectie van het Nationaal Museum van Wereldculturen, maar ook uit diverse online bronnen, zoals etnografische publicaties en veilingsites, welke zijn verzameld en beoordeeld door de conservatoren van Museon.

**Beelden trainingsmodel**

Foto’s van de maskers zijn meestal frontaal, ‘en face’, maar soms ook schuin, liggend.
De achtergrondkleur varieert van wit tot heel donker, en van lichtgroen tot oranjerood.

![Masker](/images/kavat-mask.jpg)

**Trainingsdata**

De dataset bestaat uit maskers met een veelheid aan functies, meestal ritueel, maar ook bepaalde dans- en theatermaskers maken deel uit van de set.

Disclaimer: Beeldherkenning vindt plaats op basis van visuele kenmerken van de maskers. Soms komen dezelfde kenmerken in verschillende culturen voor, dan zal het resultaat minder precies of zelfs onjuist zijn. Op basis van beeldherkenning vallen geen uitspraken te doen over de echtheid en de waarde van een masker.

