Voor dit model waarmee Papilionoidea worden herkend op het niveau van de subfamilies is niet getraind op gedetermineerde soorten, maar op de label-teksten van niet-gedetermineerde, gedigitaliseerde  specimens uit de Papillotten-collectie. De data waarmee dit model getraind is, is dus generieker, anderzijds was de dataset (support) groter dan bij het model op soortniveau. 

Dit model kan gebruikt worden om de dagvlinders te sorteren op (mogelijke) subfamilies.
