Het herkenningsmodel voor de eikapsels van roggen en haaien is getraind met 234 foto’s van 11 soorten roggen en 2 soorten haaien die langs de Nederlandse kust voorkomen.
De eikapsels, die men op de Nederlandse stranden kan aantreffen, zijn afkomstig uit de collectie van Naturalis en zijn in gedroogde toestand gefotografeerd.

**Trainingsdata**: Het model is getraind met 234 foto’s van in totaal 13 klassen (kapsels van 11 soorten roggen en 2 soorten haaien).
Het minimum aantal beelden per klasse is 2. De foto’s bevatten behalve de specimen(s) ook één een of meerdere labels, een meetlat en een kleurkaart.

**Aanvullende informatie** verschijnt bij de determinaties als link naar de soort op het Nederlands soortenregister. Deze informatie over de roggen en haaien bevat eveneens informatie over de eikapsels. 

Deze informatie is ook te vinden via de **Soortzoeker Eikapsels roggen en haaien**. Voor het determineren van verse eikapsels kun je ook gebruik maken van [ObsIdentify](https://waarneming.nl/apps/obsidentify/) en van de [determinatiegids voor eikapsels van haaien, vleten en roggen](https://www.dutchsharksociety.org/wp-content/uploads/2022/01/DeGroteEikapselJacht.pdf) van de Dutch Shark Society.

Voorbeeld van foto in de trainingsdata:

![Eikapsels](/images/rog-ei.jpg)
