**Model Papilionoidea**

Voor de gevouwen vlinders in de collectie van Naturalis, de zogenaamde ‘papillotten-collectie’, zijn meerdere modellen getraind. 

De modellen zijn getraind met foto’s van gevouwen specimens van voornamelijk Zuidoost-Aziatische dagvlinders uit de collectie van Naturalis. 

Dit model is getraind op soortniveau aan de hand van door specialisten gedetermineerde specimens, en daarnaast zijn modellen op hogere taxonomische niveaus getraind aan de hand van label-teksten om collecties te kunnen sorteren op het niveau van subfamilie of genus.

**Beelden trainingsmodel**

![Papillot](/images/papillot.jpg)

Alleen de onderzijde van één van de vleugels van deze specimens is gefotografeerd, en dit model zal dan ook geen vlinders kunnen herkennen aan beelden van de bovenzijde van de vleugel. Voor bepaalde soorten zal daardoor aanvullende informatie moeten worden geraadpleegd om de soort te kunnen determineren.

De foto’s waarop het model getraind is bevatten naast de gevouwen specimens ook labels, de oorspronkelijke verpakking (papillot) en een QR-code op een Naturalis-label met het collectienummer.

**Trainingsdata**

Voor een uniforme naamgeving is gebruik gemaakt van een name resolver met behulp van GBIF.

**Soortinformatie bij de determinaties**

Aanvullende, Engelstalige soortinformatie verschijnt bij de determinaties als link, welke verwijst naar het project _Malesian Butterflies_.
