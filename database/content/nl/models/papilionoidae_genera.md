Voor dit model waarmee Papilionoidea worden herkend op het niveau van genera is niet getraind op _gedetermineerde soorten_, maar op de label-teksten van ongedetermineerde, gedigitaliseerde specimens uit de Papillotten-collectie. 

De data waarmee dit model getraind is, is dus generieker dan het model Zuidoost-Aziatische dagvlinders (Papilionoidea), anderzijds was de dataset groter dan bij het model op soortniveau. 

Dit model kan gebruikt worden om de dagvlinders te sorteren op (mogelijke) genera.
