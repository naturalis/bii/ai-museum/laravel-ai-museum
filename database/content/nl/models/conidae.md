Het herkenningsmodel voor de huisjes van kegelslakken is getraind met foto’s van 797 soorten Conidae. Het beeldmateriaal van de gedetermineerde soorten waarmee het model getraind is, is afkomstig uit vier museale collecties en één privé-collectie.

**Beelden trainingsmodel**

Het model is getraind met foto’s van Conidae tegen zowel een donkere als lichte achtergrond. Het aantal specimens op de foto varieert; veel foto’s bevatten één of twee exemplaren, maar de trainingsset bevat ook foto’s met meerdere exemplaren.

Afhankelijk van de collectie bevatten de foto’s behalve de specimen(s) ook een of meerdere labels.


**Trainingsdata**

Beelden en identificaties zijn afkomstig van:

* Naturalis Biodiversity Center
* Natuurhistorisch Museum Rotterdam
* National History Museum London (collectie GBIF)
* Muséum national d'Histoire naturelle, Paris (collectie GBIF)
* Privécollectie van Fabrice Prugnaud (Frankrijk)

Voor dit model is een minimum van twee afbeeldingen per soort (klasse) gehanteerd.

Om de wetenschappelijke naamgeving van de soorten uit de verschillende collecties te uniformeren is gebruik gemaakt van GBIF en WoRMS.

Voorbeelden van trainingsdata:

<p>
  <img src="/images/conid2.jpg" alt="Conid" style="display:inline-block; margin-right:10px;">
  <img src="/images/conus.jpg" alt="Conidae" style="display:inline-block;">
</p>
