**Wat is de maximale bestandsgrootte voor de afbeeldingen?**

De maximale bestandsgrootte voor te identificeren foto's via de *drag
and drop* is 10 MB.

**Wat betekenen de resultaten?**

Het model geeft altijd drie identificaties die het meest lijken op klassen die het model kent. De hoogte van de predictie wordt weergegeven in een getal van 0 tot 1, met een kleurcode. (groen is hoge predictie, oranje matig, rood laag).

Bij elk model is aangegeven met welke klassen het getraind is (afhankelijk van het model de soorten, genera of subfamilies dan wel typen objecten). Deze kun je opvragen door bovenaan de pagina van het betreffende model op 'alle (...) in dit model' te klikken.

**Ik weet zeker dat de determinatie niet klopt, waarom verschijnen er toch drie resultaten?**

Het model zal altijd drie resultaten genereren die het meest lijken op de ingevoerde afbeelding, ook als het een geheel ander type object betreft. 

**Hoe kan ik de resultaten verifiëren?**

Onder de resultaten verschijnt een link naar aanvullende informatie over
de soort of het type object.

**Waar vind ik aanvullende informatie over de soort of het type object dat het model suggereert?**

Onder de resultaten staan links naar aanvullende - door Naturalis gevalideerde - informatie over de soort of het type object. Een pop-up met informatie verschijnt na het aanklikken van het i-icoontje. Ook zijn er - afhankelijk van het model - links naar aanvullende informatie op bijv. [GBIF](www.gbif.org) en [Nederlands Soortenregister](www.nederlandsesoorten.nl) beschikbaar.

**Waarom krijg ik een foutmelding of geen resultaten?**

Mogelijk is het geüploade bestand corrupt, te groot of is er teveel tijd verstreken tussen upload van het bestand en reactie van de server. Check of je bestand een geldig plaatje (bijv. een jpeg) is, niet groter dan 10mb is en probeer het daarna nog een keer. Soms helpt het ook om de cache van je webbrowser te legen.
