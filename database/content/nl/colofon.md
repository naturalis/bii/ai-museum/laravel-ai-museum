Deze webapplicatie is ontwikkeld in het kader van het project *Automatische beeldherkenning als instrument voor museumcollecties: innovatieproject in de Nederlandse natuurhistorische collecties* (2019-2022).

In dit project onderzocht Naturalis Biodiversity Center samen met partners:

* [Museon-Omniversum](https://oneplanet.nl/)
* [Natuurhistorisch Museum Rotterdam](https://www.hetnatuurhistorisch.nl/)
* [Natuurmuseum Brabant](https://www.natuurmuseumbrabant.nl/)
* [Rijksmuseum](https://www.rijksmuseum.nl/nl)

de toepasbaarheid van automatische beeldherkenning op basis van *machine learning* voor museumcollecties. Daarbij lag een specifieke focus op natuurhistorische collecties.

Dit project werd mede mogelijk gemaakt door:

* [Mondriaan Fonds](https://www.mondriaanfonds.nl/)
* [Prins Bernhard Cultuurfonds](https://www.cultuurfonds.nl/)
* [Netherlands Biodiversity Information Facility (NLBIF)](https://www.nlbif.nl/)

## Projectcoördinatie

Naturalis Biodiversity Center

## Applicatieontwikkeling

Naturalis Biodiversity Center

## Herkenningsmodellen - machine learning

Naturalis Biodiversity Center

## Herkenningsmodellen - fotomateriaal voor training

* Naturalis Biodiversity Center
* Natuurhistorisch Museum Rotterdam
* Museon-Omniversum
* Collectie Fabrice Prugnaud (Frankrijk)
* National History Museum London (collectie GBIF)
* Muséum national d'Histoire naturelle, Paris (collectie GBIF)

## Teksten / aanvullende informatie

* Naturalis Biodiversity Center (natuurhistorische collecties)
* Museon-Omniversum (maskers)
