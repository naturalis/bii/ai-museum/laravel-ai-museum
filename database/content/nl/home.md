Welke soort vlinder heb ik gedigitaliseerd?

Welk roggeneikapsel heb ik op het strand gevonden?

Tot welke cultuur behoort dit masker in mijn collectie?

Deze webapplicatie, gebaseerd op kunstmatige intelligentie (specifiek *machine learning*),
kan je helpen dergelijke identificatievragen geautomatiseerd te beantwoorden.

Bij invoer zal een model *altijd* drie resultaten geven, de drie meest gelijkende klassen
dat het model bevat.
**Het model kan dus niet aangeven dat het iets niet kent.**

Om de determinaties die de modellen leveren te kunnen verifiëren,
is een link naar aanvullende informatie toegevoegd.

Op dit moment zijn er beeldherkenningsmodellen beschikbaar voor de volgende deelcollecties:

* [Zuidoost-Aziatische dagvlinders (Papilionoidea soorten niveau)](/model/papilionoidae)
* [Zuidoost-Aziatische dagvlinders (Papilionoidea genera niveau)](/model/papilionoidae_genera)
* [Zuidoost-Aziatische dagvlinders (Papilionoidea subfamilie niveau)](/model/papilionoidae_subfamilies)
* [Huisjes van kegelslakken (Conidae)](/model/conidae)
* [Maskers uit verschillende culturen](/model/maskers)
* [Maskers uit verschillende culturen (continent niveau)](/model/maskers_continent)
* [Eikapsels roggen en haaien](/model/haaien_roggen_eieren)
* [Eieren van in Nederland voorkomende vogels](/model/vogeleieren) (beperkt toegankelijk voor publiek)

Deze experimentele webapplicatie is een resultaat van het project
*Automatische beeldherkenning als instrument voor museumcollecties:*
*innovatieproject in de Nederlandse natuurhistorische collecties* (2019-2022).
Voor meer informatie over dit project, zie het colofon
en [de projectpagina op de website van Naturalis](https://www.naturalis.nl/wetenschap/beeldherkenning-als-instrument-voor-museumcollecties).
