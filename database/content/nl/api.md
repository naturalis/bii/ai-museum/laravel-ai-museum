Alle modellen zijn toegankelijk via een publiek beschikbare API:

`https://museum.api.biodiversityanalysis.nl/`

Lijst met beschikbare modellen:

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th class="col-5">Model</th>
            <th class="col-7">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>legacy-vlinders-v2</code></td>
            <td>Vlinders van de papillotten collectie met vlinders uit Zuid-Oost Azie.</td>
        </tr>
        <tr>
            <td><code>legacy-vlinder-genera-v2</code></td>
            <td>Vlinder model identificeert afbeeldingen op genera niveau.</td>
        </tr>
        <tr>
            <td><code>legacy-vlinder-subfamilie-v2</code></td>
            <td>Vlinder model identificeert afbeeldingen op subfamily niveau.</td>
        </tr>
        <tr>
            <td><code>legacy-conidae-v2</code></td>
            <td>Model voor herkennen van schelpen van kegelslakken.</td>
        </tr>
        <tr>
            <td><code>legacy-maskers-v2</code></td>
            <td>Maskers van de museon collectie.</td>
        </tr>
        <tr>
            <td><code>legacy-maskers-continent-v2</code></td>
            <td>Maskers gegroepeerd naar continent.</td>
        </tr>
        <tr>
            <td><code>legacy-shark-eggs-v2</code></td>
            <td>Haaien- en roggereieren.</td>
        </tr>
        <tr>
            <td><code>legacy-bird-eggs-v2</code></td>
            <td>Vogel eieren (niet publiek toegankelijk).</td>
        </tr>
    </tbody>
</table>


## Endpoints

De API heeft voor iedere afzonderlijk model de volgende endpoints.
In dit voorbeeld wordt gebruik gemaakt van `legacy-vlinders-v2`,
maar je kan ieder model kiezen uit bovenstaande tabel.

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th class="col-5">Description</th>
            <th class="col-1">Method</th>
            <th class="col-6">Endpoint</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Basis</td>
            <td>GET </td>
            <td><code>/</code> </td>
        </tr>
        <tr>
            <td>Ping om API the testen</td>
            <td>GET </td>
            <td><code>/legacy-vlinders-v2/ping</code></td>
        </tr>
        <tr>
            <td>Documentatie van model</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/documentation</code></td>
        </tr>
        <tr>
            <td>Lijst met endpoints</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/endpoints</code></td>
        </tr>
        <tr>
            <td>Identificeer afbeelding</td>
            <td>POST</td>
            <td><code>/legacy-vlinders-v2/identify</code></td>
        </tr>
        <tr>
            <td>Authenticatie</td>
            <td>POST</td>
            <td><code>/legacy-vlinders-v2/identify/auth</code></td>
        </tr>
        <tr>
            <td>Identificeer afbeelding (met token)</td>
            <td>POST</td>
            <td><code>/legacy-vlinders-v2/identify/token/{token}</code></td>
        </tr>
        <tr>
            <td>Klassen herkend</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/taxa</code></td>
        </tr>
        <tr>
            <td>Klassen herkend (filter)</td>
            <td>GET</td>
            <td><code>/legacy-vlinders-v2/taxa/all?id={taxon_id}</code></td>
        </tr>
    </tbody>
</table>

### Basis 

De voorpagina van de API, met wat inleidende informatie.

URL: `https://museum.api.biodiversityanalysis.nl/`

Resultaat: Html pagina over de API.

Let op, de inhoud van deze pagina is op dit moment niet up to date.
Dit wordt op een later moment opgepakt.

### /ping

Endpoint om te testen of een specifiek model online is en klaar om afbeeldingen te identificeren.
Door deze url op te vragen kan je het model een zetje geven.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/ping`

Methode: GET

Resultaat: *pong*

### /documentation

Geeft een json representatie van de documentatie en configuratie van een bepaald model.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/documentation`

Methode: GET

Resultaat: json record

### /taxa

Geeft een alfabetische lijst van alle klassen die kunnen worden herkend door het model.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/taxa`

Parameters: none

Methode: GET

### /identify

Endpoint om afbeeldingen te identificeren.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify`

Parameter: `image=` (multipart/form-data)

Methode: POST

Voorbeeld met een curl commando:

```
curl -XPOST -F "image=@butterfly.jpg" \
  https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify
```

Het antwoord bevat een veld 'predictions', dat op zijn beurt
een JSON-gecodeerde lijst van taxa die herkend worden in de geüploade afbeelding.
Elke voorspelling bestaat uit een klasse en de waarschijnlijkheid dat het om die klasse gaat (als een fractie van 1).
De API retourneert altijd de voorspellingen voor alle klassen in het model, aflopend gerangschikt op waarschijnlijkheid.


## /identify/auth

Een aantal modellen is niet publiek toegankelijk of beperkt.
Deze zijn te herkennen aan de tag 'restricted' in de uitvoer van het endpoint van de modellen.
Als u deze modellen wilt gebruiken, neem dan contact op met Naturalis.
Als u toestemming heeft om de corresponderende endpoints te gebruiken,
dan zou u een gebruikersnaam en wachtwoord moeten hebben gekregen voor toegang.
De autorisatie is geïmplementeerd als Basic Authorization.
Voorbeeld implementatie met behulp van cURL,
als u de gebruikersnaam 'my_name' en wachtwoord 'secret' heeft gekregen:

```
curl -u "my_name:secret" \
  https://museum.api.biodiversityanalysis.nl/legacy-bird-eggs-v2/identify/auth
```

### /identify/{token}

Endpoint om een afbeelding mee te identificeren, met behulp van een door Naturalis geleverd token.
Dit kan ook gebruikt worden voor modellen met beperkte toegang of beperkte aanvragen per periode.

URL: `https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify/token/{token}`

Parameters: `image=` (multipart/form-data)

Methods: POST

Voorbeeld met curl:

```
curl -XPOST -F "image=@butterfly.jpg" \
  https://museum.api.biodiversityanalysis.nl/legacy-vlinders-v2/identify/token/{token}
```
