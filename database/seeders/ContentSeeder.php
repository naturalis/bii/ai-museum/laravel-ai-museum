<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Content;
use League\CommonMark\MarkdownConverter;

class ContentSeeder extends Seeder
{
    public function run()
    {
        Content::truncate();

        $converter = app(MarkdownConverter::class);

        $pages = [
            'home' => [
                'en' => 'Automatic image recognition for museum collections',
                'nl' => 'Automatische beeldherkenning voor museumcollecties',
            ],
            'colofon' => [
                'en' => 'Colophon',
                'nl' => 'Colofon',
            ],
            'faq' => [
                'en' => 'Frequently asked questions',
                'nl' => 'Veelgestelde vragen',
            ],
            'api' => [
                'en' => 'Describing the API',
                'nl' => 'Beschrijving van de API',
            ],
        ];

        foreach ($pages as $slug => $translations) {
            $content = new Content();
            $content->slug = $slug;
            foreach ($translations as $locale => $title) {
                $content->setTranslation('title', $locale, $title);
                $markDownContent = file_get_contents(database_path("content/$locale/$slug.md"));
                $renderedContent = $converter->convertToHtml($markDownContent);
                $content->setTranslation('body', $locale, $renderedContent->getContent());
            }
            $content->save();
        }
    }
}
