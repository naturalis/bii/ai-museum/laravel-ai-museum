<?php

namespace Database\Seeders;

use App\Models\MaskEnrichment;
use Illuminate\Database\Seeder;

class MaskEnrichmentSeeder extends Seeder
{

    public function run(): void
    {
        MaskEnrichment::truncate();

        // Load the data from a JSON file
        $json = file_get_contents(database_path('content/mask_enrichments.json'));

        // Decode the JSON data to an associative array
        $enrichments = json_decode($json, true);

        // Loop through the array and create a new record for each item
        foreach ($enrichments as $enrichment) {
            MaskEnrichment::create([
                'class' => $enrichment['class'],
                'key' => $enrichment['key'],
                'url' => $enrichment['url'],
            ]);
        }
    }
}
