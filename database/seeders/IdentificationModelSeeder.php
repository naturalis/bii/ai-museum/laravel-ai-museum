<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\IdentificationModel;
use App\Models\Content;
use League\CommonMark\MarkdownConverter;

class IdentificationModelSeeder extends Seeder
{
    /**
     * Seed the database.
     */
    public function run(): void
    {
        IdentificationModel::truncate();

        $models = $this->getModels();

        foreach ($models as $model) {
            $this->createIdentificationModel($model);
        }
    }

    private function getModels(): array
    {
        return config('services.identification.multisource');
    }

    private function createIdentificationModel(array $model): void
    {
        $enrichment_services = $model['enrichments'] ?? '{}';

        $settings = [];
        if (isset($model['restricted'])) {
            $settings = ['restricted' => $model['restricted']];
        }

        IdentificationModel::create([
            'name' => $model['model'],
            'slug' => $model['selector'],
            'api_url' => $model['api'],
            'api_key' => '',
            'enrichments' => json_encode($enrichment_services),
            'identification_service' => $model['service'],
            'settings' => json_encode($settings),
            'title' => json_encode($model['title'])
        ]);

        $this->createContent($model);
    }

    private function createContent(array $model): void
    {
        $converter = app(MarkdownConverter::class);

        $content = new Content();
        $content->slug = '/model/' . $model['selector'];

        foreach(config('app.locales') as $locale) {
            $content->setTranslation('title', $locale, ucfirst($model['title'][$locale]));

            $selector = $model['selector'];
            $markDownContent = file_get_contents(database_path("content/$locale/models/$selector.md"));
            $renderedContent = $converter->convertToHtml($markDownContent);

            $content->setTranslation('body', $locale, $renderedContent->getContent());
        }

        $content->save();
    }
}
