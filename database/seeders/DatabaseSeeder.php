<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\ContentSeeder;
use Database\Seeders\IdentificationModelSeeder;
use Database\Seeders\MaskEnrichmentSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->command->info('Seeding content');
        $this->call(ContentSeeder::class);

        $this->command->info('Seeding identification models');
        $this->call(IdentificationModelSeeder::class);

        $this->command->info('Seeding mask enrichments');
        $this->call(MaskEnrichmentSeeder::class);
    }
}
