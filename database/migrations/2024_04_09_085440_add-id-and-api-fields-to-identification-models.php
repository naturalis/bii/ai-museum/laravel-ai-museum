<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('identification_models', function (Blueprint $table) {
            $table->string('name');
            $table->string('slug');
            $table->string('api_url');
            $table->string('api_key');
            $table->json('enrichments');
            $table->json('settings');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('identification_models', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('slug');
            $table->dropColumn('api_url');
            $table->dropColumn('api_key');
            $table->dropColumn('enrichments');
            $table->dropColumn('settings');
        });
    }
};
