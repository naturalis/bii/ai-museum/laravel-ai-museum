<?php

namespace Tests\Unit;

use App\Providers\IdentificationServiceProvider;
use App\Services\IdentificationService;
use Tests\TestCase;

class IdentificationServiceProviderTest extends TestCase
{
    public function testIdentificationServiceIsRegisteredCorrectly()
    {
        $provider = new IdentificationServiceProvider($this->app);

        $provider->register();
        $this->assertInstanceOf(IdentificationService::class, $this->app->make('identification.museum_v1'));
    }
}
