<?php

namespace Tests\Unit;

use App\Providers\EnrichmentServicesProvider;
use App\Services\GbifEnrichmentService;
use App\Services\LinnaeusEnrichmentService;
use Tests\TestCase;

class EnrichmentServicesProviderTest extends TestCase
{

    public function testServicesAreRegisteredCorrectly(): void
    {
        $provider = new EnrichmentServicesProvider($this->app);

        $provider->register();

        $this->assertInstanceOf(GbifEnrichmentService::class, $this->app->make('enrichment.gbif'));
        $this->assertInstanceOf(LinnaeusEnrichmentService::class, $this->app->make('enrichment.nederlandsesoorten'));
        $this->assertInstanceOf(LinnaeusEnrichmentService::class, $this->app->make('enrichment.conidae'));
        $this->assertInstanceOf(LinnaeusEnrichmentService::class, $this->app->make('enrichment.malesianbutterflies'));
    }
}
