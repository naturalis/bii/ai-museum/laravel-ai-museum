<?php


namespace Tests\Unit;

use App\Models\IdentificationModel;
use Tests\TestCase;

class IdentificationModelTest extends TestCase
{
    /**
     * Test the identificationApi method.
     *
     * @return void
     */
    public function testIdentificationApi()
    {
        $model = IdentificationModel::first();
        $api = $model->identificationApi();

        $this->assertNotNull($api);
        // Add more assertions based on what you expect the method to return
    }

    /**
     * Test the enrichmentApis method.
     *
     * @return void
     */
    public function testEnrichmentApis()
    {
        $model = IdentificationModel::first();
        $apis = $model->enrichmentApis();

        $this->assertIsArray($apis);
        // Add more assertions based on what you expect the method to return
    }
}
