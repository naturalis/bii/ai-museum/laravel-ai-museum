<?php

namespace Tests\Unit\Services;

use App\Models\MaskEnrichment;
use App\Services\MaskEnrichmentService;
use Tests\TestCase;

class MaskEnrichmentServiceTest extends TestCase
{
    protected $maskEnrichment;
    protected $enrichmentService;

    public function setUp(): void
    {
        parent::setUp();
        $this->enrichmentService = new MaskEnrichmentService();

        MaskEnrichment::where('class', 'testclass')->delete();
        $this->maskEnrichment = MaskEnrichment::create([
            'class' => 'TestClass',
            'key' => 'testclass',
            'url' => 'http://test.com',
        ]);
    }

    public function testReturnsEnrichmentDataWhenClassNameExists()
    {
        $result = $this->enrichmentService->getEnrichmentData('TestClass');

        $this->assertEquals([
            'class' => $this->maskEnrichment->class,
            'url' => $this->maskEnrichment->url,
        ], $result);
    }

    public function testReturnsNullWhenClassNameDoesNotExist()
    {
        $result = $this->enrichmentService->getEnrichmentData('NonExistentClass');

        $this->assertNull($result);
    }

    public function testHandlesClassNamesWithWhitespaceCorrectly()
    {

        $result = $this->enrichmentService->getEnrichmentData(' Test Class ');

        $this->assertEquals([
            'class' => $this->maskEnrichment->class,
            'url' => $this->maskEnrichment->url,
        ], $result);
    }
}
