<?php

namespace Tests\Unit;

use App\Providers\AppServiceProvider;
use Illuminate\Support\Facades\URL;
use League\CommonMark\MarkdownConverter;
use Tests\TestCase;

class AppServiceProviderTest extends TestCase
{
    protected $appServiceProvider;

    public function setUp(): void
    {
        parent::setUp();
        $this->appServiceProvider = new AppServiceProvider($this->app);
    }

    public function testMarkdownConverterIsRegisteredAsSingleton()
    {
        $this->appServiceProvider->register();

        $markdownConverter1 = $this->app->make(MarkdownConverter::class);
        $markdownConverter2 = $this->app->make(MarkdownConverter::class);

        $this->assertSame($markdownConverter1, $markdownConverter2);
    }

    public function testUrlSchemeIsForcedToHttpsInBootMethod()
    {
        URL::shouldReceive('forceScheme')
            ->once()
            ->with('https');

        $this->appServiceProvider->boot();
    }
}
