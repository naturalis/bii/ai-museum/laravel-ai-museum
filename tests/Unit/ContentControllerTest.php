<?php

namespace Tests\Unit;

use App\Http\Controllers\ContentController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Tests\TestCase;

class ContentControllerTest extends TestCase
{
    public function testContentIsDisplayedCorrectly()
    {
        $controller = new ContentController();

        $response = $controller->show('en', 'home');

        $this->assertEquals('Automatic image recognition for museum collections', $response->getData()['title']);
        $this->assertEquals('en', $response->getData()['locale']);
    }

    public function testContentIsNotDisplayedWhenSlugIsInvalid()
    {
        $controller = new ContentController();

        $this->expectException(ModelNotFoundException::class);
        $controller->show('en', 'invalid-slug');
    }

    public function testContentIsDisplayedInDefaultLocaleWhenLocaleIsNotProvided()
    {
        $controller = new ContentController();

        $response = $controller->show('', 'home');

        $this->assertEquals('Automatic image recognition for museum collections', $response->getData()['title']);
        $this->assertEquals(app()->getLocale(), $response->getData()['locale']);
    }
}
