<?php

namespace Tests\Unit;

use App\Models\Content;
use Tests\TestCase;

class ContentModelTest extends TestCase
{
    private $content;

    public function setUp(): void
    {
        parent::setUp();

        Content::where('slug', 'test-slug')->delete();

        $this->content = Content::create([
            'title' => ['en' => 'English Title', 'nl' => 'Nederlandse Titel'],
            'slug' => 'test-slug',
            'body' => ['en' => 'English Body', 'nl' => 'Nederlandse Broodtekst']
        ]);
    }

    public function testCanCreateContentWithTranslatableFields()
    {
        $content = Content::create([
            'title' => ['en' => 'English Title', 'nl' => 'Nederlandse Titel'],
            'slug' => 'test-slug',
            'body' => ['en' => 'English Body', 'nl' => 'Nederlandse Broodtekst']
        ]);

        $this->assertEquals('English Title', $content->getTranslation('title', 'en'));
        $this->assertEquals('Nederlandse Titel', $content->getTranslation('title', 'nl'));
        $this->assertEquals('English Body', $content->getTranslation('body', 'en'));
        $this->assertEquals('Nederlandse Broodtekst', $content->getTranslation('body', 'nl'));
        $this->assertEquals('test-slug', $content->slug);
    }

    public function testCanFindContentBySlug()
    {
        $foundContent = Content::where('slug', 'test-slug')->first();

        $this->assertNotNull($foundContent);
        $this->assertEquals($this->content->id, $foundContent->id);
    }

    public function testReturnsNullWhenContentSlugDoesNotExist()
    {
        $content = Content::where('slug', 'nonexistent-slug')->first();

        $this->assertNull($content);
    }

    public function testReturnsSlugAsRouteKeyName()
    {
        $content = new Content();

        $this->assertEquals('slug', $content->getRouteKeyName());
    }
}
