<?php

namespace Tests\Unit;

use App\Services\EnrichmentService;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class EnrichmentServiceTest extends TestCase
{
    const TEST_URL = 'http://test.com/';
    const TEST_CLASS_NAME = 'ValidClassName';

    public function testGetsEnrichmentDataForValidClassName()
    {
        Cache::shouldReceive('remember')
            ->once()
            ->andReturn(['data' => 'test']);

        $enrichmentService = new EnrichmentService(self::TEST_URL);

        $response = $enrichmentService->getEnrichmentData(self::TEST_CLASS_NAME);

        $this->assertIsArray($response);
        $this->assertEquals('test', $response['data']);
    }

    public function testReturnsNullWhenNoEnrichmentDataIsAvailable()
    {
        Cache::shouldReceive('remember')
            ->once()
            ->andReturn(null);

        $enrichmentService = new EnrichmentService(self::TEST_URL);

        $response = $enrichmentService->getEnrichmentData(self::TEST_CLASS_NAME);

        $this->assertNull($response);
    }

    public function testCachesEnrichmentData()
    {
        Cache::shouldReceive('remember')
            ->once()
            ->andReturnUsing(function ($key, $ttl, $callback) {
                $this->assertEquals(EnrichmentService::class . ':' . self::TEST_CLASS_NAME, $key);
                $this->assertEquals(config('services.enrichment.cache_ttl'), $ttl);
                return $callback();
            });

        Http::fake([
            '*' => Http::response(['data' => 'test'], 200),
        ]);

        $enrichmentService = new EnrichmentService(self::TEST_URL);

        $response = $enrichmentService->getEnrichmentData(self::TEST_CLASS_NAME);

        $this->assertIsArray($response);
        $this->assertEquals('test', $response['data']);
    }
}
