<?php

namespace Tests\Unit\Services;

use App\Services\MultiSourceIdentificationService;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class MultiSourceIdentificationServiceTest extends TestCase
{
    protected $config;
    protected $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->config = [
            'api' => 'http://api.example.com/',
            'token' => 'testToken',
            'username' => 'testUser',
            'password' => 'testPass',
        ];
        $this->service = new MultiSourceIdentificationService($this->config);
    }

    public function testIsApiReachable()
    {
        Http::fake([
            'api.example.com/*' => Http::response(['error' => null], 200),
        ]);

        $this->assertTrue($this->service->isApiReachable());
    }

    public function testIsApiUnreachable()
    {
        Http::fake([
            'api.example.com/*' => Http::response(['error' => 'Not Found'], 404),
        ]);

        $this->assertFalse($this->service->isApiReachable());
    }

    public function testIdentifyImage()
    {
        Storage::fake('public');
        Storage::put('public/test.jpg', 'test');

        $predictions = [
            [
                'taxa' => [
                    'items' => [
                        [
                            'scientific_name' => 'test1',
                            'scientific_name_id_shared' => 'GBIF:1732416',
                            'probability' => '0.9'
                        ],
                        [
                            'scientific_name' => 'test2',
                            'scientific_name_id_shared' => 'GBIF:1732417',
                            'probability' => '0.1'
                        ],
                    ],
                ],
            ],
        ];
        $response = [
            'predictions' => $predictions,
        ];

        Http::fake([
            'api.example.com/*' => Http::response($response, 200),
        ]);

        $response = $this->service->identifyImage('test.jpg');

        $this->assertArrayHasKey('predictions', $response);
        $this->assertIsArray($response['predictions']);
        $this->assertArrayHasKey('class', $response['predictions'][0]);
        $this->assertArrayHasKey('prediction', $response['predictions'][0]);
        $this->assertArrayHasKey('enrichments', $response['predictions'][0]);
        $this->assertArrayHasKey('gbif', $response['predictions'][0]['enrichments']);
        $this->assertArrayHasKey('url', $response['predictions'][0]['enrichments']['gbif']);
        $this->assertEquals(
            'https://www.gbif.org/species/1732416',
            $response['predictions'][0]['enrichments']['gbif']['url']
        );
    }

    public function testIdentifyImageFailed()
    {
        Storage::fake('public');
        Storage::put('public/test.jpg', 'test');

        Http::fake([
            'api.example.com/*' => Http::response(['error' => 'Failed to identify image.'], 400),
        ]);

        $response = $this->service->identifyImage('test.jpg');

        $this->assertArrayHasKey('error', $response);
    }

    public function testRetrieveClasses()
    {
        Http::fake([
            'api.example.com/*' => Http::response(['classes' => []], 200),
        ]);

        $response = $this->service->retrieveClasses();

        $this->assertArrayHasKey('classes', $response);
    }

    public function testRetrieveClassesFailed()
    {
        Http::fake([
            'api.example.com/*' => Http::response(['error' => 'Failed to retrieve classes.'], 400),
        ]);

        $response = $this->service->retrieveClasses();

        $this->assertArrayHasKey('error', $response);
    }
}
