<?php

namespace Tests\Unit;

use App\Http\Controllers\LanguageController;
use Illuminate\Http\Request;
use Tests\TestCase;

class LanguageControllerTest extends TestCase
{

    const SET_LANGUAGE = '/setLanguage';

    public function testLanguageIsSetCorrectly()
    {
        $controller = new LanguageController();
        $request = Request::create(self::SET_LANGUAGE, 'POST', ['locale' => 'en']);

        $response = $controller->setLanguage($request);

        $this->assertEquals('en', session('locale'));
        $this->assertEquals('en', app()->getLocale());
        $this->assertEquals(['success' => true, 'language' => 'en'], $response->getData(true));
    }

    public function testLanguageIsNotSetWhenLocaleIsInvalid()
    {
        $controller = new LanguageController();
        $request = Request::create(self::SET_LANGUAGE, 'POST', ['locale' => 'english']);

        $response = $controller->setLanguage($request);

        $this->assertNull(session('locale'));
        $this->assertNotEquals('english', app()->getLocale());
        $this->assertArrayHasKey('errors', $response->getData(true));

        $request = Request::create(self::SET_LANGUAGE, 'POST', ['locale' => 'fr']);

        $response = $controller->setLanguage($request);

        $this->assertNull(session('locale'));
        $this->assertNotEquals('fr', app()->getLocale());
        $this->assertArrayHasKey('errors', $response->getData(true));
    }
}
