<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Notification;
use App\Http\Middleware\VerifyCsrfToken;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use WithFaker;

    public function testLoginPageResponse(): void
    {
        $response = $this->get('/login');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testRegisterPageResponse(): void
    {
        $response = $this->get('/register');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testRegisterPageForm(): void
    {
        $response = $this->get('/register');

        $response->assertSee('Register');
        $response->assertSee('Name');
        $response->assertSee('Email Address');
        $response->assertSee('Password');
        $response->assertSee('Confirm Password');
    }

    public function testRegisterPageSubmitForm(): void
    {
        $this->withoutMiddleware(VerifyCsrfToken::class);

        $password = $this->faker->password(minLength: 12);
        $formData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
        ];

        $response = $this->post('/register', $formData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');
    }

    public function testFailedLogin(): void
    {
        $this->withoutMiddleware(VerifyCsrfToken::class);

        $formData = [
            'email' => $this->faker->email,
            'password' => $this->faker->password,
        ];
        $response = $this->post('/login', $formData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');

        // check if user is not logged in
        $this->assertGuest();
    }

    public function testSuccessfulLoginAndLogout(): void
    {
        $this->withoutMiddleware(VerifyCsrfToken::class);

        // first create a user in the database
        $password = $this->faker->password(minLength: 12);
        $formData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
        ];
        $this->post('/register', $formData);

        $this->assertDatabaseHas('users', [
            'email' => $formData['email'],
        ]);
        $this->post('/login', $formData);
        $this->assertAuthenticated();

        $this->post('/logout');
        $this->assertGuest();
    }

    public function testPasswordResetPageResponse(): void
    {
        $response = $this->get('/password/reset');

        $response->assertStatus(Response::HTTP_OK);

        $response->assertSee('Reset Password');
        $response->assertSee('Email Address');
    }

    public function testPasswordResetPageFormPost(): void
    {
        $this->withoutMiddleware(VerifyCsrfToken::class);

        $email = $this->faker->email;
        $formData = [
            'email' => $email,
        ];
        $response = $this->post('/password/email', $formData);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/');

        // first create a user in the database
        $password = $this->faker->password(minLength: 12);
        $formData = [
            'name' => $this->faker->name,
            'email' => $email,
            'password' => $password,
            'password_confirmation' => $password,
        ];
        $this->post('/register', $formData);
        $this->post('/logout');
    }

    public function testPasswordResetToken(): void
    {
        Notification::fake();

        $this->withoutMiddleware(VerifyCsrfToken::class);

        $user = User::factory()->create([
            'email' => $this->faker->email,
        ]);

        $this->post('/password/email', ['email' => $user->email]);

        // Assert a notification was sent to the user
        Notification::assertSentTo($user, ResetPassword::class, function ($notification) use (&$token) {
            // Extract the token from the notification
            $token = $notification->token;
            return true;
        });
        $this->assertTrue(isset($token));
    }
}
