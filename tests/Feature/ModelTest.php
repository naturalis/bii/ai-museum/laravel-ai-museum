<?php

namespace Tests\Feature;

use App\Http\Middleware\VerifyCsrfToken;
use App\Models\IdentificationModel;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ModelTest extends TestCase
{
    protected $model;

    public function setUp(): void
    {
        parent::setUp();

        $this->model = IdentificationModel::first();
    }

    private function getModelUri(): string
    {
        return route('model', ['model' => $this->model->slug]);
    }

    private function getModelClassesUri(): string
    {
        return route('classes', ['model' => $this->model->slug]);
    }

    public function testRecognitionModelPage(): void
    {
        $response = $this->get($this->getModelUri());

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testRecognitionModelClassesPage(): void
    {
        $response = $this->get($this->getModelClassesUri());

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testRecognitionModelNotExisting(): void
    {
        $response = $this->get(route('model', ['model' => 'not-existing']));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testRecognitionModelUpload(): void
    {
        // Prepare a fake image
        Storage::fake('public/images');
        $file = UploadedFile::fake()->image('test.jpg');

        // Call the receiveImage method without CSRF token
        $response = $this->withoutMiddleware(VerifyCsrfToken::class)
                 ->json('POST', $this->getModelUri(), [
                     'image' => $file,
                 ]);

        // Assert the response is a JSON response and has predictions
        $response->assertStatus(Response::HTTP_OK)->assertJson([
            'predictions' => true,
            'enriched' => true
        ]);

        $result = $response->json();

        $hasEnrichments = false;
        foreach ($result['predictions'] as $prediction) {
            $this->assertArrayHasKey('class', $prediction);
            $this->assertArrayHasKey('prediction', $prediction);
            $hasEnrichments = $hasEnrichments || array_key_exists('enrichments', $prediction);
        }
        $this->assertTrue($hasEnrichments, 'At least one prediction should have enrichments');
    }
}
