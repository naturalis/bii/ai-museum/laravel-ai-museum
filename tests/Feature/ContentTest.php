<?php

namespace Tests\Feature;

use App\Models\Content;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ContentTest extends TestCase
{
    use WithFaker;

    protected $content;

    public function setUp(): void
    {
        parent::setUp();

        $this->content = Content::first();
    }

    private function getContentUri(string $language): string
    {
        return route('content', ['locale' => $language, 'content' => $this->content->slug]);
    }
    /**
     * Test home page response.
     */
    public function testHomePageResponse(): void
    {
        $response = $this->get('/');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testContentPage(): void
    {
        $response = $this->get($this->getContentUri('en'));
        $response->assertStatus(Response::HTTP_OK);

        $response = $this->get($this->getContentUri('nl'));
        $response->assertStatus(Response::HTTP_OK);
    }

    public function testNotExistingPage(): void
    {
        $response = $this->get(route('content', ['locale' => 'en', 'slug' => 'not-existing']));
        $response->assertStatus(Response::HTTP_NOT_FOUND);

        $response = $this->get(route('content', ['locale' => 'fr', 'slug' => 'not-existing']));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
