<!doctype html>
<html lang="{{ str_replace('_', '-', session()->get('locale') ?? app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Specimen Identification') }} @isset($title) | {{ $title }} @endisset</title>

    <link rel="icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon" />

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body id="app" class="d-flex flex-column">
    <nav class="navbar navbar-expand-md shadow-sm">
        <div class="container">
            <a href="{{ url('/') }}">
                <img src="{{ asset('images/logo--white.png') }}" alt="{{ config('app.name', 'Specimen Identification') }}" class="naturalis-logo">
            </a>
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Specimen Identification') }}
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown" aria-expanded="false">{{ __('Models') }}</a>
                        <ul class="dropdown-menu">
                            @foreach ($models as $model)
                                <li>
                                    <a class="dropdown-item" href="{{ $model->slug }}">{{ $model->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('content', ['locale' => $locale, 'slug' => 'faq']) }}">{{ __('FAQ') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('content', ['locale' => $locale, 'slug' => 'api']) }}">{{ __('API') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('content', ['locale' => $locale, 'slug' => 'colofon']) }}">{{ __('Colophon') }}</a>
                    </li>
                    <!-- Authentication Links -->
                    @auth
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endauth
                    <li>
                        <theme-mode-switch></theme-mode-switch>
                        <language-switch></language-switch>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="content-wrapper">
        <main class="py-4 flex-grow-1">
            @yield('content')
        </main>

        <footer class="footer py-3">
          <div class="container d-flex align-items-end flex-grow-1">
              <a href="{{ url('https://www.naturalis.nl') }}" class="m-1">
                  <img src="{{ asset('images/logo--red.svg') }}" alt="{{ config('app.name', 'AIMuseum') }}" class="logo naturalis">
              </a>
              <a href="{{ url('https://www.hetnatuurhistorisch.nl') }}">
                  <img class="logo nmr" alt="Natuurhistorisch Museum Rotterdam" src="{{ asset('images/logo_Het_Natuurhistorisch.jpeg') }}">
              </a>
              <a href="{{ url('https://www.natuurmuseumbrabant.nl') }}">
                  <img class="logo nmb" alt="Natuurmuseum Brabant" src="{{ asset('images/logo_nmb.png') }}">
              </a>
              <a href="{{ url('https://www.rijksmuseum.nl') }}">
                  <img class="logo rijks" alt="Rijksmuseum" src="{{ asset('images/rijksmuseum-logo.png') }}">
              </a>
              <a href="{{ url('https://oneplanet.nl') }}">
                  <img class="logo museon" alt="Museon-Omniversum" src="{{ asset('images/Museon-Omniversum_Logo_colour_RGB.png') }}">
              </a>
          </div>
        <div class="container d-flex align-items-end flex-grow-1 mt-3 text-black">
            {{ __('Also made possible by:') }}
        </div>
        <div class="container d-flex align-items-end flex-grow-1 mt-2">
            <a href="{{ url('https://www.nlbif.nl/') }}">
                <img src="{{ asset('images/nlbif.png') }}" alt="NLBIF" class="logo nlbif">
            </a>
            <a href="{{ url('https://www.cultuurfonds.nl/') }}">
                <img src="{{ asset('images/prins_bernhard.png') }}" alt="Prins Bernhard Fonds" class="logo bernhard">
            </a>
            <a href="{{ url('https://www.mondriaanfonds.nl/') }}">
                <img src="{{ asset('images/mondriaan.png') }}" alt="Mondriaan Stichting" class="logo mondriaan">
            </a>
        </div>
        </footer>
    </div>
</body>
</html>
