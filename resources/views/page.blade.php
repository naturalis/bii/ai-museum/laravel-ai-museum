@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center content">
        <h1>{!! $title !!}</h1>
        <div>{!! $body !!}</div>
    </div>
</div>
@endsection
