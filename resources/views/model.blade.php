@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center content">
        <h1>{{ $title }}</h1>
        @if(!$apiReachable)
            <div class="alert alert-warning">
                {{ __('The API is currently unavailable. Please try again later.') }}
            </div>
        @endif
        @if($restricted)
            <div class="alert alert-warning">
                {{ __('This API is restricted.') }}
            </div>
        @endif
        @if ($body)
            <div>{!! $body !!}</div>
        @endif
    </div>
</div>
@if($apiReachable && !$restricted)
<div class="container mt-4" id="image-recognition">
    <div class="row justify-content-center">
        <div class="card">
            <div class="card-header">
                {{__('Identify your specimen by uploading an image')}}
            </div>
            <div class="card-body">
                <image-upload></image-upload>
            </div>
            <div class="card-footer">
                <a href="{{ route('classes', $model->slug) }}">
                    {{ __('Classes recognized by this model') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
