@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-left content">
        <h1>{{ $title }}</h1>
        <a href="{{ route('model', $model->slug) }}">
           {{ __('Back to model') }}
        </a>
        @if (isset($error))
            <div class="alert alert-danger">
                {{ $error }}
            </div>
        @endif
        @if (isset($classes['classes']))
            <div class="container">
                <table class="table table-striped table-bordered mt-4">
                    <tbody>
                    @foreach ($classes['classes'] as $class)
                        <tr>
                            <td>{{ $class }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
@endsection
