import {createI18n} from "vue-i18n";
export const i18n = createI18n({
  locale: 'nl',
  fallbackLocale: 'en',
  messages: {
    en: {
      message: {
        noPredictions: 'No results',
        error: 'Error',
        identify: 'Identifying image...',
        class: 'Class',
        score: 'Score',
        information: 'Information',
      },
      source: {
          gbif: 'GBIF',
          nederlandsesoorten: 'Dutch Species Register',
          malesianbutterflies: 'Malesian Butterflies',
          masks: 'Museon/Omniversum',
          conidae: 'Conidae',
      }
    },
    nl: {
      message: {
        noPredictions: 'Geen resultaten',
        error: 'Er is een fout opgetreden',
        identify: 'Afbeelding identificeren...',
        class: 'Klasse',
        score: 'Score',
        information: 'Informatie',
      },
      source: {
        gbif: 'GBIF',
        nederlandsesoorten: 'Nederlands Soortenregister',
        malesianbutterflies: 'Maleisische Vlinders',
        masks: 'Museon/Omniversum',
        conidae: 'Conidae',
      }
    }
  }
})
