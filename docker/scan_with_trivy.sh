#!/bin/sh
set -eu

# Check if no arguments are provided
if [ $# -eq 0 ]; then
  echo "No image to scan because no image provided."
  exit 1
fi

wget \
  --header "PRIVATE-TOKEN: $TRIVYIGNORE_ACCESS_TOKEN" https://gitlab.com/api/v4/projects/41791412/repository/files/trivyignore/raw?ref=main \
  --output-document=.trivyignore

trivy \
  --version

mkdir -p .trivy
# shellcheck disable=SC2046,SC2143
if [ ! -f .trivy/db/trivy.db ] || [ $(find .trivy/ -name "trivy.db" -mtime +1 | grep "trivy.db") ] ; then
    echo "Download the trivy db!";
    rm -rf .trivy/db;
    trivy image --cache-dir .trivy/ --download-db-only --db-repository "$TRIVY_DB_REPOSITORY";
fi

for image in "$@"; do
  trivy \
    --cache-dir .trivy/ \
    image \
    "$CI_REGISTRY_IMAGE"/"$image":build_"$CI_COMMIT_REF_SLUG"
done

for image in "$@"; do
  trivy \
    --cache-dir .trivy/ \
    image \
    --ignore-unfixed \
    --skip-files /usr/local/bin/gosu \
    --exit-code 1 \
    --severity HIGH,CRITICAL \
    "$CI_REGISTRY_IMAGE"/"$image":build_"$CI_COMMIT_REF_SLUG"
done
